#!/bin/bash

# Run spring boot app in background
mvn clean spring-boot:run -Dspring-boot.run.jvmArguments="--enable-preview" -Ddelete-schema-files=true &

while inotifywait -r ./src/main/java -e modify,create,delete,move; do
  mvn compile -Ddelete-schema-files=true;
done


