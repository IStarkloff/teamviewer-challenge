package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.service.dto.IEntityDto;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class GetAllExpectedResultDto<TDto extends IEntityDto> {

  private List<TDto> expectedResults;
  private String errorMessage;
}
