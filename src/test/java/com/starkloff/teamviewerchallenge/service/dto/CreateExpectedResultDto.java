package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.service.dto.IEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateExpectedResultDto<TDto extends IEntityDto> {

  private TDto dtoToCreate;
  private TDto expectedResult;
  private String errorMessage;
}
