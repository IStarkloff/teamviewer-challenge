package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.rsql_config.dto.FieldsRequest;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

@Data
@AllArgsConstructor
public class GetFilteredExpectedResultDto<TDto extends IEntityDto> {

  private PageDto<TDto> expectedResults;
  private FieldsRequest fieldsRequest;
  private Sort sort;
  private String search;
  private String errorMessage;
}
