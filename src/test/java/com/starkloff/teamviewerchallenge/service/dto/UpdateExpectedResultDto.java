package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.service.dto.IEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class UpdateExpectedResultDto<TDto extends IEntityDto> {

  private TDto dtoToUpdate;
  private TDto expectedResult;
  private String errorMessage;
}
