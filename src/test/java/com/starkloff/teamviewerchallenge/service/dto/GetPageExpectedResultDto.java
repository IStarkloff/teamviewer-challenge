package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.rsql_config.dto.FieldsRequest;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

@Data
@AllArgsConstructor
public class GetPageExpectedResultDto<TDto extends IEntityDto> {

  private PageDto<TDto> expectedResults;
  private Sort sort;
  private FieldsRequest fieldsRequest;
  private String errorMessage;
}
