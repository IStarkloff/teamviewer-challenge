package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.service.dto.IEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class UpdateNotFoundExpectedResultDto<TDto extends IEntityDto> {

  private TDto dtoToUpdate;
  private String errorMessage;
}
