package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.service.dto.IEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateWithIdExpectedResultDto<TDto extends IEntityDto> {

  private TDto dtoToCreate;
  private String errorMessage;
}
