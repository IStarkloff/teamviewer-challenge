package com.starkloff.teamviewerchallenge.service.dto;

import com.starkloff.base.service.dto.IEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class GetByIdExpectedResultDto<TDto extends IEntityDto> {

  private long id;
  private TDto expectedResult;
  private String errorMessage;
}
