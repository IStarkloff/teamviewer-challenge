package com.starkloff.teamviewerchallenge.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class DeleteExpectedResultDto {

  private long idToDelete;
  private String errorMessage;
}
