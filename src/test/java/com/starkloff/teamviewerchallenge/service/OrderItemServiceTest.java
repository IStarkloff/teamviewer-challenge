package com.starkloff.teamviewerchallenge.service;

import com.starkloff.base.rsql_config.rsql_builder.RSQLBuilder;
import com.starkloff.base.service.dto.PageDto;
import com.starkloff.teamviewerchallenge.service.dto.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

@SpringBootTest
public class OrderItemServiceTest
  extends EntityServiceTest<OrderItemDto, OrderItemService> {

  List<OrderItemDto> allOrderItems = List.of(
    new OrderItemDto(
      1,
      new BigDecimal("800.00"),
      new OrderDto(1, null, LocalDateTime.parse("2024-02-16T23:02:33.000")),
      new ProductDto(
        1,
        new BigDecimal("100.00"),
        "Pro Staff 97",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      8,
      LocalDateTime.parse("2024-02-16T23:02:33.000")
    ),
    new OrderItemDto(
      2,
      new BigDecimal("800.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        1,
        new BigDecimal("100.00"),
        "Pro Staff 97",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      8,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      3,
      new BigDecimal("300.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        2,
        new BigDecimal("150.00"),
        "Pro Staff RF97 Autograph",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      2,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      4,
      new BigDecimal("180.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        3,
        new BigDecimal("90.00"),
        "Blade 98",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      2,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      5,
      new BigDecimal("800.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        4,
        new BigDecimal("80.00"),
        "Blade 104",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      10,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      6,
      new BigDecimal("800.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        5,
        new BigDecimal("100.00"),
        "Ultra 100",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      8,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      7,
      new BigDecimal("400.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        6,
        new BigDecimal("50.00"),
        "Ultra Tour",
        "Wilson",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      8,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      8,
      new BigDecimal("80.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        7,
        new BigDecimal("10.00"),
        "Pure Aero",
        "Babolat",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      8,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      9,
      new BigDecimal("40.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        8,
        new BigDecimal("40.00"),
        "Pure Aero Lite",
        "Babolat",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      1,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    ),
    new OrderItemDto(
      10,
      new BigDecimal("300.00"),
      new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
      new ProductDto(
        9,
        new BigDecimal("30.00"),
        "Pure Drive",
        "Babolat",
        LocalDateTime.parse("2024-02-16T22:55:09.000")
      ),
      10,
      LocalDateTime.parse("2024-02-16T23:08:21.000")
    )
  );

  @Override
  protected List<GetByIdExpectedResultDto<OrderItemDto>> getByIdTestExpectedResults() {
    return List.of(
      new GetByIdExpectedResultDto<>(
        1,
        new OrderItemDto(
          1,
          new BigDecimal("800.00"),
          new OrderDto(1, null, LocalDateTime.parse("2024-02-16T23:02:33.000")),
          new ProductDto(
            1,
            new BigDecimal("100.00"),
            "Pro Staff 97",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          8,
          LocalDateTime.parse("2024-02-16T23:02:33.000")
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetByIdNotFoundExpectedResultDto> getByIdNotFoundTestExpectedResults() {
    return List.of(
      new GetByIdNotFoundExpectedResultDto(
        1000,
        "It should throw a NoSuchElementException"
      )
    );
  }

  @Override
  protected List<GetByIdsExpectedResultDto<OrderItemDto>> getByIdsTestExpectedResults() {
    return List.of(
      new GetByIdsExpectedResultDto<>(
        List.of(1L, 2L),
        List.of(
          new OrderItemDto(
            1,
            new BigDecimal("800.00"),
            new OrderDto(
              1,
              null,
              LocalDateTime.parse("2024-02-16T23:02:33.000")
            ),
            new ProductDto(
              1,
              new BigDecimal("100.00"),
              "Pro Staff 97",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            8,
            LocalDateTime.parse("2024-02-16T23:02:33.000")
          ),
          new OrderItemDto(
            2,
            new BigDecimal("800.00"),
            new OrderDto(
              2,
              null,
              LocalDateTime.parse("2024-02-16T23:08:21.000")
            ),
            new ProductDto(
              1,
              new BigDecimal("100.00"),
              "Pro Staff 97",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            8,
            LocalDateTime.parse("2024-02-16T23:08:21.000")
          )
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetAllExpectedResultDto<OrderItemDto>> getAllTestExpectedResults() {
    return List.of(
      new GetAllExpectedResultDto<>(
        allOrderItems,
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetPageExpectedResultDto<OrderItemDto>> getPageTestExpectedResults() {
    return List.of(
      new GetPageExpectedResultDto<>(
        new PageDto<>(10, 1, 1, 10, allOrderItems),
        Sort.unsorted(),
        null,
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetFilteredExpectedResultDto<OrderItemDto>> getFilteredTestExpectedResults() {
    return List.of(
      new GetFilteredExpectedResultDto<>(
        new PageDto<>(
          4,
          1,
          1,
          10,
          List.of(
            new OrderItemDto(
              1,
              new BigDecimal("800.00"),
              new OrderDto(
                1,
                null,
                LocalDateTime.parse("2024-02-16T23:02:33.000")
              ),
              new ProductDto(
                1,
                new BigDecimal("100.00"),
                "Pro Staff 97",
                "Wilson",
                LocalDateTime.parse("2024-02-16T22:55:09.000")
              ),
              8,
              LocalDateTime.parse("2024-02-16T23:02:33.000")
            ),
            new OrderItemDto(
              2,
              new BigDecimal("800.00"),
              new OrderDto(
                2,
                null,
                LocalDateTime.parse("2024-02-16T23:08:21.000")
              ),
              new ProductDto(
                1,
                new BigDecimal("100.00"),
                "Pro Staff 97",
                "Wilson",
                LocalDateTime.parse("2024-02-16T22:55:09.000")
              ),
              8,
              LocalDateTime.parse("2024-02-16T23:08:21.000")
            ),
            new OrderItemDto(
              3,
              new BigDecimal("300.00"),
              new OrderDto(
                2,
                null,
                LocalDateTime.parse("2024-02-16T23:08:21.000")
              ),
              new ProductDto(
                2,
                new BigDecimal("150.00"),
                "Pro Staff RF97 Autograph",
                "Wilson",
                LocalDateTime.parse("2024-02-16T22:55:09.000")
              ),
              2,
              LocalDateTime.parse("2024-02-16T23:08:21.000")
            ),
            new OrderItemDto(
              6,
              new BigDecimal("800.00"),
              new OrderDto(
                2,
                null,
                LocalDateTime.parse("2024-02-16T23:08:21.000")
              ),
              new ProductDto(
                5,
                new BigDecimal("100.00"),
                "Ultra 100",
                "Wilson",
                LocalDateTime.parse("2024-02-16T22:55:09.000")
              ),
              8,
              LocalDateTime.parse("2024-02-16T23:08:21.000")
            )
          )
        ),
        null,
        Sort.unsorted(),
        RSQLBuilder.create().is("product.price").greaterOrEqualTo(100).query(),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<CreateExpectedResultDto<OrderItemDto>> createTestExpectedResults() {
    return null;
  }

  @Override
  protected List<CreateWithIdExpectedResultDto<OrderItemDto>> createWithIdTestExpectedResults() {
    return List.of(
      new CreateWithIdExpectedResultDto<>(
        new OrderItemDto(
          12,
          new BigDecimal("1000.00"),
          new OrderDto(2, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
          new ProductDto(
            5,
            new BigDecimal("100.00"),
            "Ultra 100",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          10,
          null
        ),
        "It should throw a ValidationException"
      )
    );
  }

  @Override
  protected List<UpdateExpectedResultDto<OrderItemDto>> updateTestExpectedResults() {
    return List.of(
      new UpdateExpectedResultDto<>(
        new OrderItemDto(
          4,
          new BigDecimal("200.00"),
          new OrderDto(1, null, null),
          new ProductDto(
            3,
            new BigDecimal("90.00"),
            "Blade 98",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          2,
          LocalDateTime.parse("2024-02-16T23:08:21.000")
        ),
        new OrderItemDto(
          4,
          new BigDecimal("200.00"),
          new OrderDto(1, null, LocalDateTime.parse("2024-02-16T23:08:21.000")),
          new ProductDto(
            3,
            new BigDecimal("90.00"),
            "Blade 98",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          2,
          LocalDateTime.parse("2024-02-16T23:08:21.000")
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<UpdateNotFoundExpectedResultDto<OrderItemDto>> updateWithIdNotFoundTestExpectedResults() {
    return List.of(
      new UpdateNotFoundExpectedResultDto<>(
        new OrderItemDto(
          124,
          new BigDecimal("200.00"),
          new OrderDto(2, null, null),
          new ProductDto(
            3,
            new BigDecimal("90.00"),
            "Blade 98",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          2,
          LocalDateTime.parse("2024-02-16T23:08:21.000")
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<DeleteExpectedResultDto> deleteTestExpectedResults() {
    return List.of(
      new DeleteExpectedResultDto(1, "The expected result is different")
    );
  }

  @Override
  protected List<DeleteExpectedResultDto> deleteWithIdNotFoundTestExpectedResults() {
    return List.of(
      new DeleteExpectedResultDto(
        1000,
        "It should throw a NoSuchElementException"
      )
    );
  }
}
