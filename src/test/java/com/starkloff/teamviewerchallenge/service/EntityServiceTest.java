package com.starkloff.teamviewerchallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import com.starkloff.teamviewerchallenge.service.dto.*;
import jakarta.validation.ValidationException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public abstract class EntityServiceTest<TDto extends IEntityDto, TService extends EntityService<TDto>> {

  protected DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
    "yyyy-MM-dd:HH:mm:ss"
  );

  @Autowired
  protected TService entityService;

  @Test
  public final void getByIdTest() {
    List<GetByIdExpectedResultDto<TDto>> getByIdExpectedResults = getByIdTestExpectedResults() !=
      null
      ? getByIdTestExpectedResults()
      : new ArrayList<>();

    if (!getByIdExpectedResults.isEmpty()) {
      for (GetByIdExpectedResultDto<TDto> getByIdExpectedResult : getByIdExpectedResults) {
        TDto result = entityService.getById(getByIdExpectedResult.getId());

        assertEquals(
          getByIdExpectedResult.getExpectedResult(),
          result,
          getByIdExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void getByIdNotFoundTest() {
    List<GetByIdNotFoundExpectedResultDto> getByIdNotFoundExpectedResults = getByIdNotFoundTestExpectedResults() !=
      null
      ? getByIdNotFoundTestExpectedResults()
      : new ArrayList<>();

    if (!getByIdNotFoundExpectedResults.isEmpty()) {
      for (GetByIdNotFoundExpectedResultDto getByIdNotFoundExpectedResult : getByIdNotFoundExpectedResults) {
        assertThrows(
          NoSuchElementException.class,
          () -> entityService.getById(getByIdNotFoundExpectedResult.getId()),
          getByIdNotFoundExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void getByIdsTest() {
    List<GetByIdsExpectedResultDto<TDto>> getByIdsExpectedResults = getByIdsTestExpectedResults() !=
      null
      ? getByIdsTestExpectedResults()
      : new ArrayList<>();

    if (!getByIdsExpectedResults.isEmpty()) {
      for (GetByIdsExpectedResultDto<TDto> getByIdsExpectedResult : getByIdsExpectedResults) {
        List<TDto> result = entityService.getByIds(
          getByIdsExpectedResult.getIds()
        );

        assertEquals(
          getByIdsExpectedResult.getExpectedResults(),
          result,
          getByIdsExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void getAllTest() {
    List<GetAllExpectedResultDto<TDto>> getAllExpectedResults = getAllTestExpectedResults() !=
      null
      ? getAllTestExpectedResults()
      : new ArrayList<>();

    if (!getAllExpectedResults.isEmpty()) {
      for (GetAllExpectedResultDto<TDto> getAllExpectedResult : getAllExpectedResults) {
        List<TDto> results = entityService.getAll();

        assertEquals(
          getAllExpectedResult.getExpectedResults(),
          results,
          getAllExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void getPageTest() {
    List<GetPageExpectedResultDto<TDto>> getPageExpectedResults = getPageTestExpectedResults() !=
      null
      ? getPageTestExpectedResults()
      : new ArrayList<>();

    if (!getPageExpectedResults.isEmpty()) {
      for (GetPageExpectedResultDto<TDto> getPageExpectedResult : getPageExpectedResults) {
        PageDto<TDto> results = entityService.getPage(
          getPageExpectedResult.getFieldsRequest(),
          PageRequest.of(
            getPageExpectedResult.getExpectedResults().getPageNumber() - 1,
            getPageExpectedResult.getExpectedResults().getPageSize(),
            getPageExpectedResult.getSort()
          )
        );

        assertEquals(
          getPageExpectedResult.getExpectedResults(),
          results,
          getPageExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void getFilteredTest() {
    List<GetFilteredExpectedResultDto<TDto>> getFilteredExpectedResults = getFilteredTestExpectedResults() !=
      null
      ? getFilteredTestExpectedResults()
      : new ArrayList<>();

    if (!getFilteredExpectedResults.isEmpty()) {
      for (GetFilteredExpectedResultDto<TDto> getFilteredExpectedResult : getFilteredExpectedResults) {
        PageDto<TDto> results = entityService.getFiltered(
          getFilteredExpectedResult.getSearch(),
          getFilteredExpectedResult.getFieldsRequest(),
          PageRequest.of(
            getFilteredExpectedResult.getExpectedResults().getPageNumber() - 1,
            getFilteredExpectedResult.getExpectedResults().getPageSize(),
            getFilteredExpectedResult.getSort()
          )
        );

        assertEquals(
          getFilteredExpectedResult.getExpectedResults(),
          results,
          getFilteredExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  @Transactional
  public void createTest() {
    List<CreateExpectedResultDto<TDto>> createExpectedResults = createTestExpectedResults() !=
      null
      ? createTestExpectedResults()
      : new ArrayList<>();

    if (!createExpectedResults.isEmpty()) {
      for (CreateExpectedResultDto<TDto> createExpectedResult : createExpectedResults) {
        TDto result = entityService.create(
          createExpectedResult.getDtoToCreate()
        );

        assertEquals(
          createExpectedResult.getExpectedResult(),
          result,
          createExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void createWithIdTest() {
    List<CreateWithIdExpectedResultDto<TDto>> createWithIdExpectedResults = createWithIdTestExpectedResults() !=
      null
      ? createWithIdTestExpectedResults()
      : new ArrayList<>();

    if (!createWithIdExpectedResults.isEmpty()) {
      for (CreateWithIdExpectedResultDto<TDto> tDtoCreateWithIdExpectedResult : createWithIdExpectedResults) {
        assertThrows(
          ValidationException.class,
          () ->
            entityService.create(
              tDtoCreateWithIdExpectedResult.getDtoToCreate()
            ),
          tDtoCreateWithIdExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  @Transactional
  public void updateTest() {
    List<UpdateExpectedResultDto<TDto>> updateExpectedResults = updateTestExpectedResults() !=
      null
      ? updateTestExpectedResults()
      : new ArrayList<>();

    if (!updateExpectedResults.isEmpty()) {
      for (UpdateExpectedResultDto<TDto> updateExpectedResult : updateExpectedResults) {
        TDto result = entityService.update(
          updateExpectedResult.getDtoToUpdate()
        );

        assertEquals(
          updateExpectedResult.getExpectedResult(),
          result,
          updateExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void updateWithIdNotFoundTest() {
    List<UpdateNotFoundExpectedResultDto<TDto>> updateNotFoundExpectedResults = updateWithIdNotFoundTestExpectedResults() !=
      null
      ? updateWithIdNotFoundTestExpectedResults()
      : new ArrayList<>();

    if (!updateNotFoundExpectedResults.isEmpty()) {
      for (UpdateNotFoundExpectedResultDto<TDto> updateNotFoundExpectedResult : updateNotFoundExpectedResults) {
        assertThrows(
          NoSuchElementException.class,
          () ->
            entityService.update(updateNotFoundExpectedResult.getDtoToUpdate()),
          updateNotFoundExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  @Transactional
  public void deleteTest() {
    List<DeleteExpectedResultDto> deleteExpectedResults = deleteTestExpectedResults() !=
      null
      ? deleteTestExpectedResults()
      : new ArrayList<>();

    if (!deleteExpectedResults.isEmpty()) {
      for (DeleteExpectedResultDto deleteExpectedResult : deleteExpectedResults) {
        entityService.deleteById(deleteExpectedResult.getIdToDelete());

        assertThrows(
          NoSuchElementException.class,
          () -> entityService.getById(deleteExpectedResult.getIdToDelete()),
          deleteExpectedResult::getErrorMessage
        );
      }
    }
  }

  @Test
  public final void deleteWithIdNotFoundTest() {
    List<DeleteExpectedResultDto> deleteExpectedResults = deleteWithIdNotFoundTestExpectedResults() !=
      null
      ? deleteWithIdNotFoundTestExpectedResults()
      : new ArrayList<>();

    if (!deleteExpectedResults.isEmpty()) {
      for (DeleteExpectedResultDto deleteExpectedResult : deleteExpectedResults) {
        assertThrows(
          NoSuchElementException.class,
          () -> entityService.deleteById(deleteExpectedResult.getIdToDelete()),
          deleteExpectedResult::getErrorMessage
        );
      }
    }
  }

  protected abstract List<GetByIdExpectedResultDto<TDto>> getByIdTestExpectedResults();

  protected abstract List<GetByIdNotFoundExpectedResultDto> getByIdNotFoundTestExpectedResults();

  protected abstract List<GetByIdsExpectedResultDto<TDto>> getByIdsTestExpectedResults();

  protected abstract List<GetAllExpectedResultDto<TDto>> getAllTestExpectedResults();

  protected abstract List<GetPageExpectedResultDto<TDto>> getPageTestExpectedResults();

  protected abstract List<GetFilteredExpectedResultDto<TDto>> getFilteredTestExpectedResults();

  protected abstract List<CreateExpectedResultDto<TDto>> createTestExpectedResults();

  protected abstract List<CreateWithIdExpectedResultDto<TDto>> createWithIdTestExpectedResults();

  protected abstract List<UpdateExpectedResultDto<TDto>> updateTestExpectedResults();

  protected abstract List<UpdateNotFoundExpectedResultDto<TDto>> updateWithIdNotFoundTestExpectedResults();

  protected abstract List<DeleteExpectedResultDto> deleteTestExpectedResults();

  protected abstract List<DeleteExpectedResultDto> deleteWithIdNotFoundTestExpectedResults();
}
