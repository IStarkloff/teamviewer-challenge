package com.starkloff.teamviewerchallenge.service;

import com.starkloff.base.rsql_config.dto.FieldsRequest;
import com.starkloff.base.rsql_config.rsql_builder.RSQLBuilder;
import com.starkloff.base.service.dto.PageDto;
import com.starkloff.teamviewerchallenge.model.Product;
import com.starkloff.teamviewerchallenge.service.dto.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

@SpringBootTest
public class ProductServiceTest
  extends EntityServiceTest<ProductDto, ProductService> {

  @Override
  protected List<GetByIdExpectedResultDto<ProductDto>> getByIdTestExpectedResults() {
    return List.of(
      new GetByIdExpectedResultDto<>(
        1,
        new ProductDto(
          1,
          new BigDecimal("100.00"),
          "Pro Staff 97",
          "Wilson",
          LocalDateTime.parse("2024-02-16T22:55:09.000")
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetByIdNotFoundExpectedResultDto> getByIdNotFoundTestExpectedResults() {
    return List.of(
      new GetByIdNotFoundExpectedResultDto(
        100L,
        "It should throw a NoSuchElementException"
      )
    );
  }

  @Override
  protected List<GetByIdsExpectedResultDto<ProductDto>> getByIdsTestExpectedResults() {
    return List.of(
      new GetByIdsExpectedResultDto<>(
        List.of(1L, 2L),
        List.of(
          new ProductDto(
            1,
            new BigDecimal("100.00"),
            "Pro Staff 97",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            2,
            new BigDecimal("150.00"),
            "Pro Staff RF97 Autograph",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          )
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetAllExpectedResultDto<ProductDto>> getAllTestExpectedResults() {
    return List.of(
      new GetAllExpectedResultDto<>(
        List.of(
          new ProductDto(
            1,
            new BigDecimal("100.00"),
            "Pro Staff 97",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            2,
            new BigDecimal("150.00"),
            "Pro Staff RF97 Autograph",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            3,
            new BigDecimal("90.00"),
            "Blade 98",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            4,
            new BigDecimal("80.00"),
            "Blade 104",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            5,
            new BigDecimal("100.00"),
            "Ultra 100",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            6,
            new BigDecimal("50.00"),
            "Ultra Tour",
            "Wilson",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            7,
            new BigDecimal("10.00"),
            "Pure Aero",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            8,
            new BigDecimal("40.00"),
            "Pure Aero Lite",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            9,
            new BigDecimal("30.00"),
            "Pure Drive",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            10,
            new BigDecimal("40.00"),
            "Pure Strike 98",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            11,
            new BigDecimal("60.00"),
            "Pure Drive Lite",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            12,
            new BigDecimal("20.00"),
            "Pure Strike 100",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          ),
          new ProductDto(
            13,
            new BigDecimal("10.00"),
            "Pure Control Tour",
            "Babolat",
            LocalDateTime.parse("2024-02-16T22:55:09.000")
          )
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetPageExpectedResultDto<ProductDto>> getPageTestExpectedResults() {
    return List.of(
      new GetPageExpectedResultDto<>(
        new PageDto<>(
          13,
          2,
          1,
          10,
          List.of(
            new ProductDto(
              1,
              new BigDecimal("100.00"),
              "Pro Staff 97",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              2,
              new BigDecimal("150.00"),
              "Pro Staff RF97 Autograph",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              3,
              new BigDecimal("90.00"),
              "Blade 98",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              4,
              new BigDecimal("80.00"),
              "Blade 104",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              5,
              new BigDecimal("100.00"),
              "Ultra 100",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              6,
              new BigDecimal("50.00"),
              "Ultra Tour",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              7,
              new BigDecimal("10.00"),
              "Pure Aero",
              "Babolat",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              8,
              new BigDecimal("40.00"),
              "Pure Aero Lite",
              "Babolat",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              9,
              new BigDecimal("30.00"),
              "Pure Drive",
              "Babolat",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              10,
              new BigDecimal("40.00"),
              "Pure Strike 98",
              "Babolat",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            )
          )
        ),
        Sort.unsorted(),
        null,
        "The expected result is different"
      ),
      new GetPageExpectedResultDto<>(
        new PageDto<>(
          13,
          2,
          1,
          10,
          List.of(
            new ProductDto(
              2,
              new BigDecimal("150.00"),
              "Pro Staff RF97 Autograph",
              null,
              null
            ),
            new ProductDto(
              1,
              new BigDecimal("100.00"),
              "Pro Staff 97",
              null,
              null
            ),
            new ProductDto(
              5,
              new BigDecimal("100.00"),
              "Ultra 100",
              null,
              null
            ),
            new ProductDto(3, new BigDecimal("90.00"), "Blade 98", null, null),
            new ProductDto(4, new BigDecimal("80.00"), "Blade 104", null, null),
            new ProductDto(
              11,
              new BigDecimal("60.00"),
              "Pure Drive Lite",
              null,
              null
            ),
            new ProductDto(
              6,
              new BigDecimal("50.00"),
              "Ultra Tour",
              null,
              null
            ),
            new ProductDto(
              8,
              new BigDecimal("40.00"),
              "Pure Aero Lite",
              null,
              null
            ),
            new ProductDto(
              10,
              new BigDecimal("40.00"),
              "Pure Strike 98",
              null,
              null
            ),
            new ProductDto(9, new BigDecimal("30.00"), "Pure Drive", null, null)
          )
        ),
        Sort.by(Sort.Order.desc("price"), Sort.Order.asc("name")),
        new FieldsRequest(List.of("id", "name", "price")),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<GetFilteredExpectedResultDto<ProductDto>> getFilteredTestExpectedResults() {
    return List.of(
      new GetFilteredExpectedResultDto<>(
        new PageDto<>(
          2,
          1,
          1,
          10,
          List.of(
            new ProductDto(
              1,
              new BigDecimal("100.00"),
              "Pro Staff 97",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            ),
            new ProductDto(
              2,
              new BigDecimal("150.00"),
              "Pro Staff RF97 Autograph",
              "Wilson",
              LocalDateTime.parse("2024-02-16T22:55:09.000")
            )
          )
        ),
        null,
        Sort.unsorted(),
        "name=='*Pro Staff*'",
        "The expected result is different"
      ),
      new GetFilteredExpectedResultDto<>(
        new PageDto<>(
          2,
          1,
          1,
          10,
          List.of(
            new ProductDto(1, null, "Pro Staff 97", null, null),
            new ProductDto(2, null, "Pro Staff RF97 Autograph", null, null)
          )
        ),
        new FieldsRequest(List.of("id", "name")),
        Sort.unsorted(),
        RSQLBuilder.create().is("name").like("*Pro Staff*").query(),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<CreateExpectedResultDto<ProductDto>> createTestExpectedResults() {
    return List.of(
      new CreateExpectedResultDto<>(
        new ProductDto(
          new BigDecimal("100.00"),
          "Pro Staff 100",
          "Wilson",
          null
        ),
        new ProductDto(
          14,
          new BigDecimal("100.00"),
          "Pro Staff 100",
          "Wilson",
          LocalDateTime.parse(
            dateTimeFormatter.format(LocalDateTime.now(ZoneOffset.UTC)),
            dateTimeFormatter
          )
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<CreateWithIdExpectedResultDto<ProductDto>> createWithIdTestExpectedResults() {
    return List.of(
      new CreateWithIdExpectedResultDto<>(
        new ProductDto(
          1,
          new BigDecimal("100.00"),
          "Pro Staff 100",
          "Wilson",
          LocalDateTime.parse("2024-02-16T22:55:09.000")
        ),
        "It should throw a ValidationException"
      )
    );
  }

  @Override
  protected List<UpdateExpectedResultDto<ProductDto>> updateTestExpectedResults() {
    return List.of(
      new UpdateExpectedResultDto<>(
        new ProductDto(
          1,
          new BigDecimal("100.00"),
          "Pro Staff 100",
          "Wilson",
          null
        ),
        new ProductDto(
          1,
          new BigDecimal("100.00"),
          "Pro Staff 100",
          "Wilson",
          LocalDateTime.parse("2024-02-16T22:55:09.000")
        ),
        "The expected result is different"
      )
    );
  }

  @Override
  protected List<UpdateNotFoundExpectedResultDto<ProductDto>> updateWithIdNotFoundTestExpectedResults() {
    return List.of(
      new UpdateNotFoundExpectedResultDto<>(
        new ProductDto(
          1000,
          new BigDecimal("100.00"),
          "Pro Staff 100",
          "Wilson",
          LocalDateTime.parse("2024-02-16T22:55:09.000")
        ),
        "It should throw a NoSuchElementException"
      )
    );
  }

  @Override
  protected List<DeleteExpectedResultDto> deleteTestExpectedResults() {
    return List.of(
      new DeleteExpectedResultDto(10, "The expected result is different")
    );
  }

  @Override
  protected List<DeleteExpectedResultDto> deleteWithIdNotFoundTestExpectedResults() {
    return List.of(
      new DeleteExpectedResultDto(
        1000,
        "It should throw a NoSuchElementException"
      )
    );
  }
}
