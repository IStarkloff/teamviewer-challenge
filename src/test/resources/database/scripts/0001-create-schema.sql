
create table "order" (
                         created_at timestamp(6),
                         id bigserial not null,
                         primary key (id)
);

create table order_item (
                            amount integer not null,
                            price numeric(38,2),
                            created_at timestamp(6),
                            id bigserial not null,
                            order_id bigint not null,
                            product_id bigint not null,

                            primary key (id)
);

create table product (
                         price numeric(38,2),
                         created_at timestamp(6),
                         id bigserial not null,
                         brand varchar(255),
                         name varchar(255),
                         primary key (id)
);

alter table if exists order_item
    add constraint FKs234mi6jususbx4b37k44cipy
        foreign key (order_id)
            references "order";

alter table if exists order_item
    add constraint FK551losx9j75ss5d6bfsqvijna
        foreign key (product_id)
            references product;
