
    alter table if exists order_item 
       drop constraint if exists FKs234mi6jususbx4b37k44cipy;

    alter table if exists order_item 
       drop constraint if exists FK551losx9j75ss5d6bfsqvijna;

    drop table if exists "order" cascade;

    drop table if exists order_item cascade;

    drop table if exists product cascade;
