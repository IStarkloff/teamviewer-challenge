package com.starkloff.teamviewerchallenge.model;

import com.starkloff.base.model.BaseEntity;
import com.starkloff.base.model.ICreatedAtEntity;
import com.starkloff.base.model.IUpdatedAtEntity;
import jakarta.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Product extends BaseEntity implements ICreatedAtEntity {

  private BigDecimal price;
  private String name;
  private String brand;
  private LocalDateTime createdAt;

  @Override
  public Class<Product> realType() {
    return Product.class;
  }
}
