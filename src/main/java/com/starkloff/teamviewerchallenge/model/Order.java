package com.starkloff.teamviewerchallenge.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.starkloff.base.model.BaseEntity;
import com.starkloff.base.model.ICreatedAtEntity;
import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "\"order\"")
public class Order extends BaseEntity implements ICreatedAtEntity {

  @OneToMany(
    cascade = { CascadeType.MERGE, CascadeType.REMOVE },
    mappedBy = "order",
    fetch = FetchType.EAGER
  )
  @JsonManagedReference
  private List<OrderItem> orderItems;

  private LocalDateTime createdAt;

  @Override
  public Class<Order> realType() {
    return Order.class;
  }
}
