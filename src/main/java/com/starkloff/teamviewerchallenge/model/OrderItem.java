package com.starkloff.teamviewerchallenge.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.starkloff.base.model.BaseEntity;
import com.starkloff.base.model.ICreatedAtEntity;
import com.starkloff.base.model.IUpdatedAtEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class OrderItem extends BaseEntity implements ICreatedAtEntity {

  private BigDecimal price;

  @ManyToOne(optional = false)
  @JoinColumn(name = "order_id")
  @JsonBackReference
  private Order order;

  @ManyToOne(optional = false)
  private Product product;

  private int amount;
  private LocalDateTime createdAt;

  @Override
  public Class<OrderItem> realType() {
    return OrderItem.class;
  }
}
