package com.starkloff.teamviewerchallenge.data;

import com.starkloff.base.data.EntityRepository;
import com.starkloff.teamviewerchallenge.model.Order;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface OrderRepository extends EntityRepository<Order> {}
