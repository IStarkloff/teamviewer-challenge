package com.starkloff.teamviewerchallenge.data;

import com.starkloff.base.data.EntityRepository;
import com.starkloff.teamviewerchallenge.model.OrderItem;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface OrderItemRepository extends EntityRepository<OrderItem> {}
