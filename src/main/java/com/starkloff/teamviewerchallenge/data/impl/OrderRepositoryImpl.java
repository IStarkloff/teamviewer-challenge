package com.starkloff.teamviewerchallenge.data.impl;

import com.starkloff.base.data.impl.EntityRepositoryImpl;
import com.starkloff.teamviewerchallenge.data.OrderRepository;
import com.starkloff.teamviewerchallenge.model.Order;
import jakarta.persistence.EntityManager;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl
  extends EntityRepositoryImpl<Order>
  implements OrderRepository {

  public OrderRepositoryImpl(EntityManager entityManager) {
    super(Order.class, entityManager);
  }
}
