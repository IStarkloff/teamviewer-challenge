package com.starkloff.teamviewerchallenge.data.impl;

import com.starkloff.base.data.impl.EntityRepositoryImpl;
import com.starkloff.teamviewerchallenge.data.ProductRepository;
import com.starkloff.teamviewerchallenge.model.Product;
import jakarta.persistence.EntityManager;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRepositoryImpl
  extends EntityRepositoryImpl<Product>
  implements ProductRepository {

  public ProductRepositoryImpl(EntityManager entityManager) {
    super(Product.class, entityManager);
  }
}
