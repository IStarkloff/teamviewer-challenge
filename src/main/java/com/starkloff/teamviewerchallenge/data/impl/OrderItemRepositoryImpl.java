package com.starkloff.teamviewerchallenge.data.impl;

import com.starkloff.base.data.impl.EntityRepositoryImpl;
import com.starkloff.teamviewerchallenge.data.OrderItemRepository;
import com.starkloff.teamviewerchallenge.model.OrderItem;
import jakarta.persistence.EntityManager;
import org.springframework.stereotype.Repository;

@Repository
public class OrderItemRepositoryImpl
  extends EntityRepositoryImpl<OrderItem>
  implements OrderItemRepository {

  public OrderItemRepositoryImpl(EntityManager entityManager) {
    super(OrderItem.class, entityManager);
  }
}
