package com.starkloff.teamviewerchallenge.data;

import com.starkloff.base.data.EntityRepository;
import com.starkloff.teamviewerchallenge.model.Product;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ProductRepository extends EntityRepository<Product> {}
