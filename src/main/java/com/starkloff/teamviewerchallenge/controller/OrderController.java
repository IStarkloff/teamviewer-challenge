package com.starkloff.teamviewerchallenge.controller;

import com.starkloff.base.controller.CustomValidatorController;
import com.starkloff.base.controller.EntityController;
import com.starkloff.teamviewerchallenge.service.OrderService;
import com.starkloff.teamviewerchallenge.service.dto.OrderDto;
import com.starkloff.teamviewerchallenge.validation.OrderValidator;
import lombok.NonNull;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/orders")
public class OrderController
  extends EntityController<OrderDto, OrderService>
  implements CustomValidatorController<OrderDto, OrderValidator> {

  private final OrderValidator orderValidator;

  public OrderController(
    OrderService entityService,
    OrderValidator orderValidator
  ) {
    super(entityService);
    this.orderValidator = orderValidator;
  }

  @Override
  public @NonNull Class<OrderDto> getDtoType() {
    return OrderDto.class;
  }

  @Override
  public @NonNull OrderValidator getValidator() {
    return orderValidator;
  }
}
