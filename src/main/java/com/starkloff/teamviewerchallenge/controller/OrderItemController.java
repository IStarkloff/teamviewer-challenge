package com.starkloff.teamviewerchallenge.controller;

import com.starkloff.base.controller.CustomValidatorController;
import com.starkloff.base.controller.EntityController;
import com.starkloff.teamviewerchallenge.service.OrderItemService;
import com.starkloff.teamviewerchallenge.service.dto.OrderItemDto;
import com.starkloff.teamviewerchallenge.validation.OrderItemValidator;
import lombok.NonNull;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/order-items")
public class OrderItemController
  extends EntityController<OrderItemDto, OrderItemService>
  implements CustomValidatorController<OrderItemDto, OrderItemValidator> {

  private final OrderItemValidator orderItemValidator;

  public OrderItemController(
    OrderItemService entityService,
    OrderItemValidator orderItemValidator
  ) {
    super(entityService);
    this.orderItemValidator = orderItemValidator;
  }

  @Override
  public @NonNull Class<OrderItemDto> getDtoType() {
    return OrderItemDto.class;
  }

  @Override
  public @NonNull OrderItemValidator getValidator() {
    return orderItemValidator;
  }
}
