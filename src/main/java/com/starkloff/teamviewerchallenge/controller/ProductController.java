package com.starkloff.teamviewerchallenge.controller;

import com.starkloff.base.controller.EntityController;
import com.starkloff.teamviewerchallenge.service.ProductService;
import com.starkloff.teamviewerchallenge.service.dto.ProductDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/products")
public class ProductController
  extends EntityController<ProductDto, ProductService> {

  public ProductController(ProductService entityService) {
    super(entityService);
  }
}
