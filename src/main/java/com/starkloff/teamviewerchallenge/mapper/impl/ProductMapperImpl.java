package com.starkloff.teamviewerchallenge.mapper.impl;

import com.starkloff.base.mapper.impl.EntityMapperImpl;
import com.starkloff.teamviewerchallenge.model.Product;
import com.starkloff.teamviewerchallenge.service.dto.ProductDto;
import org.springframework.stereotype.Component;

@Component
public class ProductMapperImpl extends EntityMapperImpl<Product, ProductDto> {

  public ProductMapperImpl() {
    super(Product.class, ProductDto.class);
  }
}
