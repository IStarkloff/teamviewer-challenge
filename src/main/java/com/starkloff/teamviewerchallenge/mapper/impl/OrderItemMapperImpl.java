package com.starkloff.teamviewerchallenge.mapper.impl;

import com.starkloff.base.mapper.impl.EntityMapperImpl;
import com.starkloff.teamviewerchallenge.model.OrderItem;
import com.starkloff.teamviewerchallenge.service.dto.OrderItemDto;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OrderItemMapperImpl
  extends EntityMapperImpl<OrderItem, OrderItemDto> {

  @Override
  protected void mapToDto(
    OrderItem source,
    OrderItemDto destination,
    List<String> fieldsToMap
  ) {
    // TODO Improve this HOTFIX
    var additionalFieldsToMap = Arrays.asList(
      "id",
      "order.id",
      "order.createdAt",
      "price",
      "product",
      "amount",
      "createdAt"
    );
    if (fieldsToMap != null) {
      additionalFieldsToMap.addAll(fieldsToMap);
    }
    super.mapToDto(source, destination, additionalFieldsToMap);
  }

  public OrderItemMapperImpl() {
    super(OrderItem.class, OrderItemDto.class);
  }
}
