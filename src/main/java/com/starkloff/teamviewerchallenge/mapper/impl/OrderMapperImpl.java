package com.starkloff.teamviewerchallenge.mapper.impl;

import com.starkloff.base.mapper.impl.EntityMapperImpl;
import com.starkloff.teamviewerchallenge.model.Order;
import com.starkloff.teamviewerchallenge.service.dto.OrderDto;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OrderMapperImpl extends EntityMapperImpl<Order, OrderDto> {

  public OrderMapperImpl() {
    super(Order.class, OrderDto.class);
  }

  @Override
  protected void mapToDto(
    Order source,
    OrderDto destination,
    List<String> fieldsToMap
  ) {
    // TODO Improve this HOTFIX
    var additionalFieldsToMap = Arrays.asList(
      "id",
      "createdAt",
      "orderItems.id",
      "orderItems.price",
      "orderItems.product",
      "orderItems.amount",
      "orderItems.createdAt"
    );
    if (fieldsToMap != null) {
      additionalFieldsToMap.addAll(fieldsToMap);
    }
    super.mapToDto(source, destination, additionalFieldsToMap);
  }
}
