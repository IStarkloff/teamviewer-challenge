package com.starkloff.teamviewerchallenge.service.impl;

import com.starkloff.base.service.impl.EntityServiceImpl;
import com.starkloff.teamviewerchallenge.data.OrderRepository;
import com.starkloff.teamviewerchallenge.mapper.impl.OrderMapperImpl;
import com.starkloff.teamviewerchallenge.model.Order;
import com.starkloff.teamviewerchallenge.service.OrderItemService;
import com.starkloff.teamviewerchallenge.service.OrderService;
import com.starkloff.teamviewerchallenge.service.dto.OrderDto;
import com.starkloff.teamviewerchallenge.service.dto.OrderItemDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderServiceImpl
  extends EntityServiceImpl<Order, OrderDto, OrderRepository, OrderMapperImpl>
  implements OrderService {

  private final OrderItemService orderItemService;

  protected OrderServiceImpl(
    OrderRepository repository,
    OrderMapperImpl mapper,
    OrderItemService orderItemService
  ) {
    super(repository, mapper);
    this.orderItemService = orderItemService;
  }

  // TODO This should be done by CascadeType.PERSIST on the @OneToMany annotation, but it does not work :(
  @Override
  @Transactional
  public OrderDto create(OrderDto entityDto) {
    var order = super.create(entityDto);
    for (OrderItemDto orderItem : order.getOrderItems()) {
      orderItem.setOrder(order);
    }
    order.setOrderItems(orderItemService.createAll(order.getOrderItems()));
    return order;
  }
}
