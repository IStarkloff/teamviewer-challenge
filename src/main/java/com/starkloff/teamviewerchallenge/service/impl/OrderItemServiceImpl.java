package com.starkloff.teamviewerchallenge.service.impl;

import com.starkloff.base.service.impl.EntityServiceImpl;
import com.starkloff.teamviewerchallenge.data.OrderItemRepository;
import com.starkloff.teamviewerchallenge.mapper.impl.OrderItemMapperImpl;
import com.starkloff.teamviewerchallenge.model.OrderItem;
import com.starkloff.teamviewerchallenge.service.OrderItemService;
import com.starkloff.teamviewerchallenge.service.dto.OrderItemDto;
import org.springframework.stereotype.Service;

@Service
public class OrderItemServiceImpl
  extends EntityServiceImpl<OrderItem, OrderItemDto, OrderItemRepository, OrderItemMapperImpl>
  implements OrderItemService {

  protected OrderItemServiceImpl(
    OrderItemRepository repository,
    OrderItemMapperImpl mapper
  ) {
    super(repository, mapper);
  }
}
