package com.starkloff.teamviewerchallenge.service.impl;

import com.starkloff.base.service.impl.EntityServiceImpl;
import com.starkloff.teamviewerchallenge.data.ProductRepository;
import com.starkloff.teamviewerchallenge.mapper.impl.ProductMapperImpl;
import com.starkloff.teamviewerchallenge.model.Product;
import com.starkloff.teamviewerchallenge.service.ProductService;
import com.starkloff.teamviewerchallenge.service.dto.ProductDto;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl
  extends EntityServiceImpl<Product, ProductDto, ProductRepository, ProductMapperImpl>
  implements ProductService {

  protected ProductServiceImpl(
    ProductRepository repository,
    ProductMapperImpl mapper
  ) {
    super(repository, mapper);
  }

  @Override
  public ProductDto update(ProductDto entityDto) {
    return super.update(entityDto);
  }
}
