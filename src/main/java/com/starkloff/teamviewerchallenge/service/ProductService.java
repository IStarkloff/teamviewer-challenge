package com.starkloff.teamviewerchallenge.service;

import com.starkloff.base.service.EntityService;
import com.starkloff.teamviewerchallenge.service.dto.ProductDto;

public interface ProductService extends EntityService<ProductDto> {}
