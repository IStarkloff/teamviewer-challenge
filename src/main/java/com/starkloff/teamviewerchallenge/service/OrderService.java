package com.starkloff.teamviewerchallenge.service;

import com.starkloff.base.service.EntityService;
import com.starkloff.teamviewerchallenge.service.dto.OrderDto;

public interface OrderService extends EntityService<OrderDto> {}
