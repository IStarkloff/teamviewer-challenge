package com.starkloff.teamviewerchallenge.service;

import com.starkloff.base.service.EntityService;
import com.starkloff.teamviewerchallenge.service.dto.OrderItemDto;

public interface OrderItemService extends EntityService<OrderItemDto> {}
