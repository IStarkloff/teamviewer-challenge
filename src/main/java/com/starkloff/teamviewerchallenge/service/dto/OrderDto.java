package com.starkloff.teamviewerchallenge.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.starkloff.base.service.dto.BaseEntityDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDto extends BaseEntityDto {

  @NotEmpty(message = "The order must have at least 1 item")
  private List<OrderItemDto> orderItems;

  @Schema(accessMode = Schema.AccessMode.READ_ONLY)
  private LocalDateTime createdAt;

  public OrderDto(
    long id,
    List<OrderItemDto> orderItems,
    LocalDateTime createdAt
  ) {
    super(id);
    this.orderItems = orderItems;
    this.createdAt = createdAt;
  }
}
