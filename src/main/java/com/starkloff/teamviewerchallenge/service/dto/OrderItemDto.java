package com.starkloff.teamviewerchallenge.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.starkloff.base.service.dto.BaseEntityDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderItemDto extends BaseEntityDto {

  @Min(value = 0, message = "The price must be a positive number")
  private BigDecimal price;

  @NotNull(message = "You must specify an order")
  private OrderDto order;

  @NotNull(message = "You must specify a product")
  private ProductDto product;

  @Min(value = 1, message = "The amount must be greater than zero")
  private Integer amount;

  @Schema(accessMode = Schema.AccessMode.READ_ONLY)
  private LocalDateTime createdAt;

  public OrderItemDto(
    long id,
    BigDecimal price,
    OrderDto order,
    ProductDto product,
    Integer amount,
    LocalDateTime createdAt
  ) {
    super(id);
    this.price = price;
    this.order = order;
    this.product = product;
    this.amount = amount;
    this.createdAt = createdAt;
  }
}
