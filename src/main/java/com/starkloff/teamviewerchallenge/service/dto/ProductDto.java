package com.starkloff.teamviewerchallenge.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.starkloff.base.service.dto.BaseEntityDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto extends BaseEntityDto {

  @Min(value = 0, message = "The price must be a positive number")
  private BigDecimal price;

  @NotBlank(message = "The product name cannot be empty")
  private String name;

  @NotBlank(message = "The product brand cannot be empty")
  private String brand;

  @Schema(accessMode = Schema.AccessMode.READ_ONLY)
  private LocalDateTime createdAt;

  public ProductDto(
    long id,
    BigDecimal price,
    String name,
    String brand,
    LocalDateTime createdAt
  ) {
    super(id);
    this.price = price;
    this.name = name;
    this.brand = brand;
    this.createdAt = createdAt;
  }
}
