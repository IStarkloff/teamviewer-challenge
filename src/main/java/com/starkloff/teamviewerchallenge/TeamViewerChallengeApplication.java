package com.starkloff.teamviewerchallenge;

import com.starkloff.base.controller.RestExceptionHandler;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = { "com.starkloff" })
@Import(RestExceptionHandler.class)
@EntityScan(
  basePackages = {
    "com.starkloff.base.model", "com.starkloff.teamviewerchallenge.model",
  }
)
@OpenAPIDefinition(
  info = @Info(
    title = "TeamViewer Challenge APIs",
    version = "0.0.1-SNAPSHOT",
    description = "This application allows users to perform CRUD (create, read, update, delete) operations on a data model representing a simple e-commerce platform."
  )
)
public class TeamViewerChallengeApplication {

  public static void main(String[] args) {
    SpringApplication.run(TeamViewerChallengeApplication.class, args);
  }
}
