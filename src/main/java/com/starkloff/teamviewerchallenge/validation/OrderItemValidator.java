package com.starkloff.teamviewerchallenge.validation;

import com.starkloff.base.validation.EntityValidator;
import com.starkloff.teamviewerchallenge.service.OrderService;
import com.starkloff.teamviewerchallenge.service.ProductService;
import com.starkloff.teamviewerchallenge.service.dto.OrderItemDto;
import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class OrderItemValidator extends EntityValidator<OrderItemDto> {

  private final OrderService orderService;
  private final ProductService productService;

  public OrderItemValidator(
    OrderService orderService,
    ProductService productService
  ) {
    super(OrderItemDto.class);
    this.orderService = orderService;
    this.productService = productService;
  }

  @Override
  protected void validateCreateAndUpdate(OrderItemDto dto, Errors errors) {
    this.validateIfProductExist(dto, errors);
    this.validateIfOrderExist(dto, errors);
  }

  public void validateIfProductExist(OrderItemDto orderItemDto, Errors errors) {
    try {
      var product = productService.getById(orderItemDto.getProduct().getId());
      orderItemDto.setProduct(product);
    } catch (NoSuchElementException e) {
      errors.rejectValue(
        "product",
        String.valueOf(HttpStatus.NOT_FOUND.series().value()),
        e.getMessage()
      );
    }
  }

  public void validateIfOrderExist(OrderItemDto orderItemDto, Errors errors) {
    try {
      if (orderItemDto.getOrder() != null) {
        var order = orderService.getById(orderItemDto.getOrder().getId());
        orderItemDto.setOrder(order);
      }
    } catch (NoSuchElementException e) {
      errors.rejectValue(
        "order",
        String.valueOf(HttpStatus.NOT_FOUND.series().value()),
        e.getMessage()
      );
    }
  }
}
