package com.starkloff.teamviewerchallenge.validation;

import com.starkloff.base.validation.EntityValidator;
import com.starkloff.teamviewerchallenge.data.ProductRepository;
import com.starkloff.teamviewerchallenge.service.dto.OrderDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

@Component
public class OrderValidator extends EntityValidator<OrderDto> {

  private final OrderItemValidator orderItemValidator;


  public OrderValidator(OrderItemValidator orderItemValidator) {
    super(OrderDto.class);
    this.orderItemValidator = orderItemValidator;
  }

  @Override
  protected void validateCreateAndUpdate(OrderDto dto, Errors errors) {
    for (int i = 0; i < dto.getOrderItems().size(); i++){
      errors.setNestedPath(STR."orderItems[\{i}]");
      orderItemValidator.validate(dto.getOrderItems().get(i), errors);
    }
  }
}
