package com.starkloff.base.rsql_config.enums;

import jakarta.persistence.criteria.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum RsqlSearchOperationToPredicate {
  LIKE {
    @Override
    @SuppressWarnings("unchecked")
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.like(builder.upper((Expression<String>) property), argument.toString().replace('*', '%').toUpperCase());
    }
  },
  NOT_LIKE {
    @Override
    @SuppressWarnings("unchecked")
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.notLike((Expression<String>) property, argument.toString().replace('*', '%'));
    }
  },
  IS_NULL {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      return builder.isNull(property);
    }
  },
  IS_NOT_NULL {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      return builder.isNotNull(property);
    }
  },
  EQUAL {
    @Override
    @SuppressWarnings("unchecked")
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      if (argument instanceof String){
        return builder.equal(builder.upper((Expression<String>) property), argument.toString().toUpperCase());
      }
      return builder.equal(property, argument);
    }
  },
  EQUAL_ENUM {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      Object enumResult = EQUAL_ENUM.getEnumResult(property, argument.toString());
      return builder.equal(property, enumResult);
    }
  },
  NOT_EQUAL {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.notEqual(property, argument);
    }
  },
  NOT_EQUAL_ENUM {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      Object enumResult = NOT_EQUAL_ENUM.getEnumResult(property, argument.toString());
      return builder.notEqual(property, enumResult);
    }
  },
  GREATER_THAN {
    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.greaterThan((Expression<Comparable>) property, (Comparable) argument);
    }
  },
  GREATER_THAN_OR_EQUAL {
    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.greaterThanOrEqualTo((Expression<Comparable>) property, (Comparable) argument);
    }
  },
  LESS_THAN {
    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.lessThan((Expression<Comparable>) property, (Comparable) argument);
    }
  },
  LESS_THAN_OR_EQUAL {
    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      Object argument = args.get(0);
      return builder.lessThanOrEqualTo((Expression<Comparable>) property, (Comparable) argument);
    }
  },
  IN {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      return property.in(args);
    }
  },

  IN_ENUM {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      List<Object> enumValues = new ArrayList<>();
      for (Object argument : args) {
        enumValues.add(IN_ENUM.getEnumResult(property, argument.toString()));
      }
      return property.in(enumValues);
    }
  },
  NOT_IN {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      return builder.not(property.in(args));
    }
  },
  NOT_IN_ENUM {
    @Override
    public Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder) {
      List<Object> enumValues = new ArrayList<>();
      for (Object argument : args) {
        enumValues.add(NOT_IN_ENUM.getEnumResult(property, argument.toString()));
      }
      return builder.not(property.in(enumValues));
    }
  };

  public abstract Predicate toPredicate(Path<?> property, List<Object> args, Root<?> root, CriteriaBuilder builder);

  private Object getEnumResult(Path<?> enumValues, String argument) {
    return Arrays
      .stream(enumValues.getJavaType().getEnumConstants())
      .filter(enumValue -> enumValue.toString().equals(argument))
      .findFirst()
      .orElseThrow(() ->
        new IllegalArgumentException(
            STR."The selected attribute is an Enum and the value you passed was not in the list of Enum values, these are the allowed values: \{Arrays.toString(enumValues.getJavaType().getEnumConstants())}"
        )
      );
  }
}
