package com.starkloff.base.rsql_config.enums;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.RSQLOperators;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public enum RsqlSearchOperation {
  EQUAL(RSQLOperators.EQUAL) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      Object argument = args.get(0);
      if (argument == null) {
        return RsqlSearchOperationToPredicate.IS_NULL.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.EQUAL.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      if (argument instanceof String && ((String) argument).contains("*")) {
        return RsqlSearchOperationToPredicate.LIKE.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      if (property.getJavaType().isEnum()) {
        return RsqlSearchOperationToPredicate.EQUAL_ENUM.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.EQUAL.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  NOT_EQUAL(RSQLOperators.NOT_EQUAL) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      Object argument = args.get(0);
      if (argument == null) {
        return RsqlSearchOperationToPredicate.IS_NOT_NULL.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.NOT_EQUAL.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      if (argument instanceof String && ((String) argument).contains("*")) {
        return RsqlSearchOperationToPredicate.NOT_LIKE.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      if (property.getJavaType().isEnum()) {
        return RsqlSearchOperationToPredicate.NOT_EQUAL_ENUM.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.NOT_EQUAL.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  GREATER_THAN(RSQLOperators.GREATER_THAN) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.GREATER_THAN.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.GREATER_THAN.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  GREATER_THAN_OR_EQUAL(RSQLOperators.GREATER_THAN_OR_EQUAL) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.GREATER_THAN_OR_EQUAL.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.GREATER_THAN_OR_EQUAL.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  LESS_THAN(RSQLOperators.LESS_THAN) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.LESS_THAN.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.LESS_THAN.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  LESS_THAN_OR_EQUAL(RSQLOperators.LESS_THAN_OR_EQUAL) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.LESS_THAN_OR_EQUAL.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.LESS_THAN_OR_EQUAL.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  IN(RSQLOperators.IN) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      if (property.getJavaType().isEnum()) {
        return RsqlSearchOperationToPredicate.IN_ENUM.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.IN.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.IN.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  },
  NOT_IN(RSQLOperators.NOT_IN) {
    @Override
    public Predicate toPredicate(
      Path<?> property,
      List<Object> args,
      Root<?> root,
      CriteriaBuilder builder
    ) {
      if (property.getJavaType().isEnum()) {
        return RsqlSearchOperationToPredicate.NOT_IN_ENUM.toPredicate(
          property,
          args,
          root,
          builder
        );
      }
      if (
        property.getJavaType() == LocalDate.class ||
        property.getJavaType() == Date.class
      ) {
        return RsqlSearchOperationToPredicate.NOT_IN.toPredicate(
          property,
          args
            .stream()
            .map(dateString -> LocalDate.parse((String) dateString))
            .collect(Collectors.toList()),
          root,
          builder
        );
      }
      return RsqlSearchOperationToPredicate.NOT_IN.toPredicate(
        property,
        args,
        root,
        builder
      );
    }
  };

  private final ComparisonOperator operator;

  private RsqlSearchOperation(ComparisonOperator operator) {
    this.operator = operator;
  }

  public static RsqlSearchOperation getSimpleOperator(
    ComparisonOperator operator
  ) {
    for (RsqlSearchOperation operation : values()) {
      if (operation.operator == operator) {
        return operation;
      }
    }
    return null;
  }

  public abstract Predicate toPredicate(
    Path<?> property,
    List<Object> args,
    Root<?> root,
    CriteriaBuilder builder
  );
}
