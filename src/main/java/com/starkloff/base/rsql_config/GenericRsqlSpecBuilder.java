package com.starkloff.base.rsql_config;

import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.LogicalNode;
import cz.jirutka.rsql.parser.ast.LogicalOperator;
import cz.jirutka.rsql.parser.ast.Node;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.springframework.data.jpa.domain.Specification;

public class GenericRsqlSpecBuilder<T> {

  public Specification<T> createSpecification(Node node) {
    if (node instanceof LogicalNode) {
      return createSpecification((LogicalNode) node);
    }
    if (node instanceof ComparisonNode) {
      return createSpecification((ComparisonNode) node);
    }
    return null;
  }

  public Specification<T> createSpecification(LogicalNode logicalNode) {
    List<Specification<T>> specs = logicalNode
      .getChildren()
      .stream()
      .map(this::createSpecification)
      .filter(Objects::nonNull)
      .toList();

    Specification<T> result = specs.get(0);
    if (logicalNode.getOperator() == LogicalOperator.AND) {
      for (int i = 1; i < specs.size(); i++) {
        result = Specification.where(result).and(specs.get(i));
      }
    } else if (logicalNode.getOperator() == LogicalOperator.OR) {
      for (int i = 1; i < specs.size(); i++) {
        result = Specification.where(result).or(specs.get(i));
      }
    }

    return result;
  }

  public Specification<T> createSpecification(ComparisonNode comparisonNode) {
    return (root, query, criteriaBuilder) -> {
      String selector = comparisonNode.getSelector();
      List<String> selectorParts = new ArrayList<>(
        Arrays.asList(selector.split("\\."))
      );
      if (selectorParts.size() > 1) {
        Join<T, ?> join = null;
        for (String selectorPart : selectorParts) {
          if (selectorParts.indexOf(selectorPart) < selectorParts.size() - 1) {
            join =
              Objects
                .requireNonNullElse(join, root)
                .join(selectorPart, JoinType.LEFT);
          }
        }
        assert join != null;
        return Specification
          .where(
            new GenericRsqlSpecification<T>(
              join.get(selectorParts.get(selectorParts.size() - 1)),
              comparisonNode.getOperator(),
              comparisonNode.getArguments()
            )
          )
          .toPredicate(root, query, criteriaBuilder);
      } else {
        return Specification
          .where(
            new GenericRsqlSpecification<T>(
              root.get(comparisonNode.getSelector()),
              comparisonNode.getOperator(),
              comparisonNode.getArguments()
            )
          )
          .toPredicate(root, query, criteriaBuilder);
      }
    };
  }
}
