package com.starkloff.base.rsql_config.rsql_builder;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;

public interface Property {
  /** Is textual property equal to given value? */
  CompleteCondition equalTo(Object value);

  /** Is textual property like given value? */
  CompleteCondition like(String value);

  /** Is textual property different from given value? */
  CompleteCondition notEqualTo(Object value);

  /** Is textual property like given value? */
  CompleteCondition notLike(String value);

  /** Is numeric property greater than given value? */
  CompleteCondition greaterThan(Object value);

  /** Is numeric property less than given value? */
  CompleteCondition lessThan(Object value);

  /** Is numeric property greater or equal to given value? */
  CompleteCondition greaterOrEqualTo(Object value);

  /** Is numeric property less or equal to given value? */
  CompleteCondition lessOrEqualTo(Object value);

  /** Is a property in the given values? */
  CompleteCondition in(Iterable<?> values);

  /** Is a property not in the given values? */
  CompleteCondition notIn(Iterable<?> values);

  /** Is a property in the given values? */
  CompleteCondition in(Object... values);

  /** Is a property not in the given values? */
  CompleteCondition notIn(Object... values);

  /** Generic */
  CompleteCondition comparesTo(ComparisonOperator op, Object value);
}
