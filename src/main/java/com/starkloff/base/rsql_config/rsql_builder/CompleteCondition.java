package com.starkloff.base.rsql_config.rsql_builder;

public interface CompleteCondition {
  /** Conjunct current expression with another */
  QueryBuilder and();

  /** shortcut for and().is() */
  Property and(String name);

  /** Disjunct current expression with another */
  QueryBuilder or();

  /** shortcut for or().is() */
  Property or(String name);

  /** Finalize condition construction and build search condition query. */
  String query();
}
