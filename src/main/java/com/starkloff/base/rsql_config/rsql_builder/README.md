# Rsql Builder

Build a Rsql (Fiql) query.

Base library: [rsql-parser](https://github.com/jirutka/rsql-parser)

## Description

RSQL is a query language for parametrized filtering of entries in RESTful APIs. It’s based on [FIQL](https://datatracker.ietf.org/doc/html/draft-nottingham-atompub-fiql-00) (Feed Item Query Language) – an URI-friendly syntax for expressing filters across the entries in an Atom Feed. FIQL is great for use in URI; there are no unsafe characters, so URL encoding is not required. On the other side, FIQL’s syntax is not very intuitive and URL encoding isn’t always that big deal, so RSQL also provides a friendlier syntax for logical operators and some of the comparison operators.

For example, you can query your resource like this: `/products?search=name=="Coca Cola"` or `/order-items?search=product.name=="*Cola*"`

## Usage

### Initialize query builder

```java
var queryBuilder = RSQLBuilder.create();
```

## Operations

### EQUAL

```java
// name=='Starkloff'
queryBuilder.is("name").equalTo("Starkloff").query();
```

### LIKE

```java
// name=='*Stark*'
queryBuilder.is("name").like("Stark").query();
```

### NOT EQUAL TO

```java
// name!='Stark'
queryBuilder.is("name").notEqualTo("Stark").query();
```

### NOT LIKE

```java
// name!='*Stark*'
queryBuilder.is("name").notLlike("Stark").query();
```

### GREATER THAN

```java
// age=gt='21'
queryBuilder.is("age").greaterThan(21).query();
```

### LESS THAN

```java
// age=lt='21'
queryBuilder.is("age").lessThan(21).query();
```

### GREATER OR EQUAL TO

```java
// age=ge='21'
queryBuilder.is("age").greaterOrEqualTo(21).query();
```

### LESS OR EQUAL TO

```java
// age=le='21'
queryBuilder.is("age").lessOrEqualTo(21).query();
```

### IN

```java
// name=in='[Hector, Aquiles, Homero]'
queryBuilder.is("name").in("Hector", "Aquiles", "Homero").query();;
```

### NOT IN

```java
// name=out='[Hector, Aquiles, Homero]'
queryBuilder.is("name").notIn("Hector", "Aquiles", "Homero").query();;
```

## Group of expressions

```java
// (name=='*TeamViewer*';(developers=ge='10',(name=='Apple',developers=le='10')))
queryBuilder.and(queryBuilder.is("name").like("TeamViewer"), queryBuilder.is("developers").greaterOrEqualTo(10)
        .or().or(queryBuilder.is("name").equalTo("Apple"), queryBuilder.is("developers").lessOrEqualTo(10))).query();
```
