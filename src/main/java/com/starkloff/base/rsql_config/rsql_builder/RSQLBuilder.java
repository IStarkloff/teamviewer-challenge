package com.starkloff.base.rsql_config.rsql_builder;

import cz.jirutka.rsql.parser.ast.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.StreamSupport;

// Based on https://github.com/diorcety/rsql
public class RSQLBuilder {

  private static final Printer DEFAULT_PRINTER = Object::toString;

  private final Printer printer;

  private class QueryBuilderImpl implements QueryBuilder {

    private final LogicalNode previous;

    public QueryBuilderImpl(LogicalNode previous) {
      this.previous = previous;
    }

    @Override
    public Property is(String property) {
      return new PropertyImpl(previous, property);
    }

    private List<Node> getNodes(Iterable<CompleteCondition> conditions) {
      ArrayList<Node> arrayList = new ArrayList<>(
        (int) StreamSupport.stream(conditions.spliterator(), false).count()
      );
      for (CompleteCondition condition : conditions) {
        arrayList.add(((CompleteConditionImpl) condition).getNode());
      }
      return arrayList;
    }

    @Override
    public CompleteCondition and(CompleteCondition... cn) {
      List<CompleteCondition> arrayList = new ArrayList<>(cn.length);
      Collections.addAll(arrayList, cn);
      return and(arrayList);
    }

    @Override
    public CompleteCondition or(CompleteCondition... cn) {
      List<CompleteCondition> arrayList = new ArrayList<>(cn.length);
      Collections.addAll(arrayList, cn);
      return or(arrayList);
    }

    @Override
    public CompleteCondition and(Iterable<CompleteCondition> conditions) {
      return new CompleteConditionImpl(
        append(previous, new AndNode(getNodes(conditions)))
      );
    }

    @Override
    public CompleteCondition or(Iterable<CompleteCondition> conditions) {
      return new CompleteConditionImpl(
        append(previous, new OrNode(getNodes(conditions)))
      );
    }
  }

  private class PropertyImpl implements Property {

    private final String name;
    private final LogicalNode previous;

    public PropertyImpl(LogicalNode previous, String name) {
      this.previous = previous;
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public CompleteCondition comparesTo(
      ComparisonOperator operator,
      Object value
    ) {
      return condition(operator, value);
    }

    public CompleteCondition equalTo(Object value) {
      return condition(RSQLOperators.EQUAL, value);
    }

    public CompleteCondition like(String value) {
      return condition(RSQLOperators.EQUAL, "*" + value + "*");
    }

    public CompleteCondition notEqualTo(Object value) {
      return condition(RSQLOperators.NOT_EQUAL, value);
    }

    public CompleteCondition notLike(String value) {
      return condition(RSQLOperators.NOT_EQUAL, "*" + value + "*");
    }

    public CompleteCondition greaterOrEqualTo(Object value) {
      return condition(RSQLOperators.GREATER_THAN_OR_EQUAL, value);
    }

    public CompleteCondition greaterThan(Object value) {
      return condition(RSQLOperators.GREATER_THAN, value);
    }

    public CompleteCondition lessOrEqualTo(Object value) {
      return condition(RSQLOperators.LESS_THAN_OR_EQUAL, value);
    }

    public CompleteCondition lessThan(Object value) {
      return condition(RSQLOperators.LESS_THAN, value);
    }

    public CompleteCondition in(Iterable<?> values) {
      return condition(RSQLOperators.IN, values);
    }

    public CompleteCondition notIn(Iterable<?> values) {
      return condition(RSQLOperators.NOT_IN, values);
    }

    @Override
    public CompleteCondition in(Object... values) {
      return in(Arrays.asList(values));
    }

    @Override
    public CompleteCondition notIn(Object... values) {
      return notIn(Arrays.asList(values));
    }

    protected CompleteCondition condition(
      ComparisonOperator operator,
      Object... values
    ) {
      ArrayList<String> arrayList = new ArrayList<String>(values.length);
      for (Object value : values) {
        arrayList.add(RSQLBuilder.this.printer.toString(value));
      }
      Node node = new ComparisonNode(operator, name, arrayList);
      return new CompleteConditionImpl(append(previous, node));
    }
  }

  private class CompleteConditionImpl implements CompleteCondition {

    private final Node node;

    public CompleteConditionImpl(Node node) {
      this.node = node;
    }

    public Node getNode() {
      return node;
    }

    @Override
    public QueryBuilder and() {
      return new QueryBuilderImpl(new AndNode(Collections.singletonList(node)));
    }

    @Override
    public Property and(String name) {
      return and().is(name);
    }

    @Override
    public QueryBuilder or() {
      return new QueryBuilderImpl(new OrNode(Collections.singletonList(node)));
    }

    @Override
    public Property or(String name) {
      return or().is(name);
    }

    @Override
    public String query() {
      return node.toString();
    }
  }

  private RSQLBuilder(Printer printer) {
    this.printer = printer;
  }

  public static QueryBuilder create() {
    return new RSQLBuilder(DEFAULT_PRINTER).getPartialCondition();
  }

  public static QueryBuilder create(Printer printer) {
    return new RSQLBuilder(printer).getPartialCondition();
  }

  private QueryBuilder getPartialCondition() {
    return new QueryBuilderImpl(null);
  }

  private static Node append(LogicalNode logicalNode, Node node) {
    if (logicalNode != null) {
      List<Node> children = logicalNode.getChildren();
      children.add(node);
      return logicalNode.withChildren(children);
    }
    return node;
  }

  public interface Printer {
    String toString(Object value);
  }
}
