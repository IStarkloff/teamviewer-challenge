package com.starkloff.base.rsql_config.rsql_builder;

public interface QueryBuilder {
  /** Get property of inspected entity type */
  Property is(String property);

  /** Conjunct multiple expressions */
  CompleteCondition and(CompleteCondition... cn);

  /** Disjunct multiple expressions */
  CompleteCondition or(CompleteCondition... cn);

  /** Conjunct multiple expressions */
  CompleteCondition and(Iterable<CompleteCondition> conditions);

  /** Disjunct multiple expressions */
  CompleteCondition or(Iterable<CompleteCondition> conditions);
}
