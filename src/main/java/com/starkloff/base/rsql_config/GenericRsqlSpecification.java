package com.starkloff.base.rsql_config;

import com.starkloff.base.rsql_config.enums.RsqlSearchOperation;
import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import jakarta.persistence.criteria.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.jpa.domain.Specification;

@Data
@AllArgsConstructor
public class GenericRsqlSpecification<T> implements Specification<T> {

  private Path<?> property;
  private ComparisonOperator operator;
  private List<String> arguments;

  @Override
  public Predicate toPredicate(
    Root<T> root,
    CriteriaQuery<?> query,
    CriteriaBuilder builder
  ) {
    List<Object> args = castArguments(root);
    return Objects
      .requireNonNull(RsqlSearchOperation.getSimpleOperator(operator))
      .toPredicate(property, args, root, builder);
  }

  private List<Object> castArguments(final Root<T> root) {
    Class<?> type = property.getJavaType();
    if (arguments.get(0).matches("\\[.+\\]")) {
      arguments =
        Arrays.asList(
          arguments
            .get(0)
            .substring(1, arguments.get(0).length() - 1)
            .split(", ")
        );
    }

    return arguments
      .stream()
      .map(arg -> {
        if (type.equals(Integer.class)) {
          return Integer.parseInt(arg);
        } else if (type.equals(Long.class)) {
          return Long.parseLong(arg);
        } else if (type.equals(Boolean.class)) {
          return Boolean.parseBoolean(arg);
        } else {
          return arg;
        }
      })
      .collect(Collectors.toList());
  }
}
