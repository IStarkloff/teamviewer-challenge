package com.starkloff.base.mapper.impl;

import com.starkloff.base.mapper.EntityMapper;
import com.starkloff.base.model.IEntity;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.util.field.FieldUtil;
import com.starkloff.base.util.field.FormattedField;
import jakarta.persistence.Tuple;
import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.lang.Nullable;

import java.util.*;

public abstract class EntityMapperImpl<TModel extends IEntity, TDto extends IEntityDto> implements EntityMapper<TModel, TDto> {
  private final Class<TModel> modelType;
  private final Class<TDto> dtoType;

  protected EntityMapperImpl(Class<TModel> modelType, Class<TDto> dtoType) {
    this.modelType = modelType;
    this.dtoType = dtoType;
  }

  @Override
  public final Class<TModel> getModelType() {
    return modelType;
  }

  @Override
  public final Class<TDto> getDtoType() {
    return dtoType;
  }

  @Override
  public final void toDto(TModel source, TDto destination, @Nullable List<String> selectedFields) {

    if (source == null) {
      throw new NullPointerException("source");
    }

    if (destination == null) {
      throw new NullPointerException("destination");
    }

    mapToDto(source, destination, selectedFields);
  }

  private record ListField(String baseField, Class<?> parentDtoType) {
  }

  protected void mapToDto(TModel source, TDto destination, List<String> fieldsToMap) {
    ModelMapper modelMapper = getModelMapper();

    if (fieldsToMap != null && !fieldsToMap.isEmpty()) {
      List<FormattedField> selectedFields = FieldUtil.formatFields(fieldsToMap, "", dtoType);
      List<ListField> fieldsInList = new ArrayList<>();

      Condition<TModel, TDto> fieldsToMapCondition = context -> {
        var path = context.getMapping().getPath();
        path = path.substring(0, path.length() - 1);
        return isFieldSelected(selectedFields, fieldsInList, path, (Class<?>) context.getParent().getGenericDestinationType(), (Class<?>) context.getGenericDestinationType());
      };

      modelMapper.getConfiguration().setPropertyCondition(fieldsToMapCondition);
    }

    modelMapper.map(source, destination);
  }

  private boolean isFieldSelected(List<FormattedField> selectedFields, List<ListField> fieldsInList, String path, Class<?> parentDestinationClass, Class<?> destinationClass) {
    Optional<FormattedField> contextField = FieldUtil.getFieldByFullPath(selectedFields, path);
    Optional<ListField> selectedListField = fieldsInList.stream().filter(listField -> listField.parentDtoType().equals(parentDestinationClass)).findFirst();
    if (Iterable.class.isAssignableFrom(destinationClass) && contextField.isPresent()) {
      if (fieldsInList.stream().noneMatch(listField -> listField.baseField().equals(path))) {
        fieldsInList.add(new ListField(path, contextField.get().dtoType()));
      }
      return true;
    } else if (selectedListField.isPresent()) {
      contextField = FieldUtil.getFieldByFullPath(selectedFields, STR."\{selectedListField.get().baseField()}.\{path}");
      return contextField.isPresent();
    } else {
      return contextField.isPresent();
    }
  }

  @Override
  public final TDto tupleToDto(Tuple tuple) {
    ModelMapper modelMapper = getModelMapper();

    Map<String, Object> maps = new HashMap<>();

    tuple.getElements().forEach(row -> {
      Object value = tuple.get(row.getAlias());
      if (value != null && row.getAlias().contains(".")) {
        String[] nestedAttributes = row.getAlias().split("\\.");
        setValueInNestedMap(maps, nestedAttributes, value);
      } else {
        maps.put(row.getAlias(), value);
      }
    });

    return modelMapper.map(maps, dtoType);
  }

  private void setValueInNestedMap(Map<String, Object> parentMap, String[] nestedAttributes, Object value) {
    String attributeName = nestedAttributes[0];
    if (!parentMap.containsKey(attributeName)) {
      parentMap.put(attributeName, new HashMap<>());
    }
    Object nestedObject = parentMap.get(attributeName);

    if (nestedAttributes.length > 2) {
      if (nestedObject instanceof Map) {
        @SuppressWarnings("unchecked") Map<String, Object> nestedMap = (Map<String, Object>) nestedObject;
        String[] remainingAttributes = Arrays.copyOfRange(nestedAttributes, 1, nestedAttributes.length);
        setValueInNestedMap(nestedMap, remainingAttributes, value);
      }
    } else if (nestedAttributes.length == 2) {
      if (nestedObject instanceof Map) {
        @SuppressWarnings({"unchecked"}) Map<String, Object> nestedMap = (Map<String, Object>) nestedObject;
        String lastAttribute = nestedAttributes[1];
        nestedMap.put(lastAttribute, value);
      } else if (nestedObject instanceof List) {
        @SuppressWarnings({"unchecked"}) List<Object> nestedList = (List<Object>) nestedObject;
        for (Object item : nestedList) {
          if (item instanceof Map) {
            @SuppressWarnings({"unchecked"}) Map<String, Object> itemMap = (Map<String, Object>) item;
            String lastAttribute = nestedAttributes[1];
            itemMap.put(lastAttribute, value);
          }
        }
      }
    }
  }

  @Override
  public final void toModel(TDto source, TModel destination, List<String> fieldsToIgnore) {

    if (source == null) {
      throw new NullPointerException("source");
    }

    if (fieldsToIgnore == null) {
      throw new NullPointerException("fieldsToIgnore");
    }

    mapToModel(source, destination, fieldsToIgnore);
  }

  protected void mapToModel(TDto source, TModel destination, List<String> fieldsToIgnore) {
    ModelMapper modelMapper = getModelMapper();

    if (fieldsToIgnore == null){
      fieldsToIgnore = List.of("createdAt", "updatedAt");
    } else {
      fieldsToIgnore.addAll(List.of("createdAt", "updatedAt"));
    }
    var finalFieldsToIgnore = fieldsToIgnore;
    Condition<TModel, TDto> fieldsToMapCondition = context -> {
      var path = context.getMapping().getPath();
      path = path.substring(0, path.length() - 1);
      // TODO Handle fields (createdAt, updatedAt) in a separate mapper per interface
      for (String skippedPath : finalFieldsToIgnore) {
        if (path.contains(skippedPath)) {
          return false;
        }
      }
      return true;
    };

    modelMapper.getConfiguration().setPropertyCondition(fieldsToMapCondition);

    modelMapper.map(source, destination);
  }

  private static ModelMapper getModelMapper() {
    ModelMapper modelMapper = new ModelMapper();

    modelMapper
        .getConfiguration()
        .setMatchingStrategy(MatchingStrategies.STRICT)
        .setSkipNullEnabled(true);
    return modelMapper;
  }
}
