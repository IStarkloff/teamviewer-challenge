package com.starkloff.base.mapper;

import com.starkloff.base.model.IEntity;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import com.starkloff.base.util.collection.Streams;
import jakarta.persistence.Tuple;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;

public interface EntityMapper<TModel extends IEntity, TDto extends IEntityDto> {
  Class<TModel> getModelType();

  Class<TDto> getDtoType();

  void toModel(TDto source, TModel destination, List<String> fieldsToIgnore);

  default void toDto(TModel source, TDto destination) {
    this.toDto(source, destination, new ArrayList());
  }

  void toDto(TModel source, TDto destination, List<String> selectedFields);

  default PageDto<TDto> toPageDto(Page<TModel> sources) {
    return toPageDto(sources, new ArrayList());
  }

  default PageDto<TDto> toPageDto(
    Page<TModel> sources,
    List<String> selectedFields
  ) {
    return new PageDto<>(
      (int) sources.getTotalElements(),
      sources.getTotalPages(),
      sources.getPageable().getPageNumber() + 1,
      sources.getPageable().getPageSize(),
      toDtos(sources.getContent(), selectedFields).collect(Collectors.toList())
    );
  }

  default TDto toDto(TModel source, List<String> selectedFields) {
    if (source == null) {
      return null;
    }

    TDto destination;

    try {
      destination = getDtoType().getDeclaredConstructor().newInstance();
    } catch (Exception ex) {
      throw new MapperException(
        String.format(
          "Error creating instance of class %s",
          getDtoType().getName()
        ),
        ex
      );
    }

    toDto(source, destination, selectedFields);

    return destination;
  }

  default Stream<TDto> toDtos(Iterable<? extends TModel> sources) {
    return Streams.from(sources).map(this::toDto);
  }

  default TDto toDto(TModel source) {
    return toDto(source, new ArrayList<>());
  }

  default Stream<TDto> toDtos(
    Iterable<? extends TModel> sources,
    List<String> selectedFields
  ) {
    return Streams.from(sources).map(source -> toDto(source, selectedFields));
  }

  default TModel toModel(TDto source) {
    return this.toModel(source, new ArrayList<>());
  }

  default TModel toModel(TDto source, List<String> fieldsToIgnore) {
    if (source == null) {
      return null;
    }
    TModel destination;

    try {
      destination = getModelType().getDeclaredConstructor().newInstance();
    } catch (Exception ex) {
      throw new MapperException(
        String.format(
          "Error creating instance of class %s",
          getModelType().getName()
        ),
        ex
      );
    }
    toModel(source, destination, fieldsToIgnore);

    return destination;
  }

  default void toModel(TDto source, TModel destination) {
    if (source == null) {
      return;
    }

    toModel(source, destination, new ArrayList<>());
  }

  default Stream<TModel> toModels(Iterable<? extends TDto> sources) {
    return Streams.from(sources).map(this::toModel);
  }

  TDto tupleToDto(Tuple tuple);

  default Stream<TDto> tuplesToDtos(Iterable<Tuple> sources) {
    return Streams.from(sources).map(this::tupleToDto);
  }

  default PageDto<TDto> tuplesToPageDto(Page<Tuple> sources) {
    return new PageDto<>(
      (int) sources.getTotalElements(),
      sources.getTotalPages(),
      sources.getPageable().getPageNumber() + 1,
      sources.getPageable().getPageSize(),
      tuplesToDtos(sources.getContent()).collect(Collectors.toList())
    );
  }
}
