package com.starkloff.base.mapper;

import com.starkloff.base.service.ServiceException;
import java.io.Serial;

public class MapperException extends ServiceException {

  @Serial
  private static final long serialVersionUID = -185781350206562176L;

  public MapperException() {}

  public MapperException(String message) {
    super(message);
  }

  public MapperException(String message, Exception innerException) {
    super(message, innerException);
  }
}
