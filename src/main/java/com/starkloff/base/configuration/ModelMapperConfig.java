package com.starkloff.base.configuration;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();

    // Configure the matching strategy for case-sensitive, same-name mapping
    modelMapper
      .getConfiguration()
      .setMatchingStrategy(MatchingStrategies.STRICT)
      .setSkipNullEnabled(true);

    return modelMapper;
  }
}
