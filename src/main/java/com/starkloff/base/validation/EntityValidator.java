package com.starkloff.base.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.starkloff.base.service.dto.IEntityDto;
import java.io.IOException;
import lombok.NonNull;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public abstract class EntityValidator<TDto extends IEntityDto>
  implements Validator {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private final Class<TDto> dtoType;
  private final EntityValidator<? super TDto> parentValidator;

  public EntityValidator(Class<TDto> dtoType) {
    this(dtoType, null);
  }

  public EntityValidator(
    Class<TDto> dtoType,
    EntityValidator<? super TDto> parentValidator
  ) {
    this.dtoType = dtoType;
    this.parentValidator = parentValidator;
  }

  protected abstract void validateCreateAndUpdate(TDto dto, Errors errors);

  @Override
  public boolean supports(@NonNull Class<?> clazz) {
    return dtoType.isAssignableFrom(clazz);
  }

  @Override
  public void validate(@NonNull Object target, @NonNull Errors errors) {
    var entityDto = dtoType.cast(target);

    if (parentValidator != null) {
      parentValidator.validate(entityDto, errors);
    }
    this.validateCreateAndUpdate(entityDto, errors);
  }

  public Class<TDto> getDtoType() {
    return dtoType;
  }

  protected static boolean isValidJsonObject(String json) {
    try {
      JsonNode jsonNode = OBJECT_MAPPER.readTree(json);
      return jsonNode instanceof ObjectNode;
    } catch (IOException ex) {
      return false;
    }
  }
}
