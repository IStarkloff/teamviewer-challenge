package com.starkloff.base.util;

import jakarta.persistence.Tuple;
import jakarta.persistence.TupleElement;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.lang.Nullable;

public class TupleUtil {

  public static <TDto> TDto parseTupleToDTO(Tuple tuple, Class<TDto> dtoType) {
    TDto dto;
    try {
      dto = dtoType.getDeclaredConstructor().newInstance();
    } catch (Exception e) {
      throw new RuntimeException("Failed to create an instance of DTO.", e);
    }

    Map<String, Integer> aliasIndices = getAliasIndices(tuple.getElements());
    for (Field field : dtoType.getDeclaredFields()) {
      String fieldName = field.getName();
      if (aliasIndices.containsKey(fieldName)) {
        Integer index = aliasIndices.get(fieldName);
        Object value = tuple.get(index);
        field.setAccessible(true);
        try {
          if (field.getType().isAssignableFrom(value.getClass())) {
            field.set(dto, value);
          } else {
            Object nestedDto = mapEntityToDTO(value, field.getType(), null);
            field.set(dto, nestedDto);
          }
        } catch (IllegalAccessException e) {
          throw new RuntimeException(
            "Failed to set value for field: " + fieldName,
            e
          );
        }
      }
    }

    return dto;
  }

  private static <T> T mapEntityToDTO(
    Object entity,
    Class<T> dtoClass,
    @Nullable Field[] selectedFields
  ) {
    T dto;
    try {
      dto = dtoClass.getDeclaredConstructor().newInstance();
    } catch (Exception e) {
      throw new RuntimeException("Failed to create an instance of DTO.", e);
    }

    Field[] dtoFields = selectedFields != null
      ? selectedFields
      : dtoClass.getDeclaredFields();
    for (Field field : dtoFields) {
      String fieldName = field.getName();
      try {
        if (
          Arrays
            .stream(entity.getClass().getDeclaredFields())
            .anyMatch(entityField -> entityField.getName().equals(fieldName))
        ) {
          Field entityField = entity.getClass().getDeclaredField(fieldName);
          entityField.setAccessible(true);
          Object value = entityField.get(entity);
          field.setAccessible(true);
          if (field.getType().isAssignableFrom(value.getClass())) {
            field.set(dto, value);
          } else {
            Object nestedDto = mapEntityToDTO(value, field.getType(), null);
            field.set(dto, nestedDto);
          }
        }
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new RuntimeException(
          "Failed to set value for field: " + fieldName,
          e
        );
      }
    }

    return dto;
  }

  private static Map<String, Integer> getAliasIndices(
    List<TupleElement<?>> elements
  ) {
    Map<String, Integer> aliasIndices = new HashMap<>();
    for (int i = 0; i < elements.size(); i++) {
      TupleElement<?> element = elements.get(i);
      String alias = element.getAlias();
      if (alias != null) {
        aliasIndices.put(alias, i);
      }
    }
    return aliasIndices;
  }
}
