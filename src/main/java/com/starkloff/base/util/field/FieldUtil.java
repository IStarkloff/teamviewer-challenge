package com.starkloff.base.util.field;

import com.starkloff.base.service.dto.IEntityDto;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FieldUtil {
  public static List<FormattedField> formatFields(
    List<String> fields,
    String fullField,
    Class<?> parentType
  ) {
    List<FormattedField> formattedFields = new ArrayList<>();

    for (String field : fields) {
      formattedFields.add(formatField(field, fullField, parentType));
    }

    return formattedFields;
  }

  public static FormattedField formatField(
    String selectedField,
    String fullField,
    Class<?> parentDtoType
  ) {
    int dotIndex = selectedField.indexOf(".");
    Class<?> fieldType;
    if (dotIndex == -1) {
      fieldType = getFieldClass(parentDtoType, selectedField);
      List<FormattedField> nestedFields = new ArrayList<>();
      fullField += fullField.isBlank() ? selectedField : STR.".\{selectedField}";
      if (IEntityDto.class.isAssignableFrom(fieldType)) {
        nestedFields.addAll(
          formatFields(getClassFields(fieldType, ""), fullField, fieldType)
        );
      }
      return new FormattedField(
        selectedField,
        nestedFields,
        fullField,
        parentDtoType,
        fieldType
      );
    } else {
      String currentField = selectedField.substring(0, dotIndex);
      String remainingField = selectedField.substring(dotIndex + 1);
      fullField += fullField.isBlank() ? currentField : STR.".\{currentField}";
      fieldType = getFieldClass(parentDtoType, currentField);
      return new FormattedField(
        currentField,
        List.of(formatField(remainingField, fullField, fieldType)),
        fullField,
        parentDtoType,
        fieldType
      );
    }
  }

  public static List<String> getClassFields(Class<?> clazz, String prefix) {
    Field[] fields = clazz.getDeclaredFields();
    List<String> nestedFieldKeys = new ArrayList<>();
    formatFieldsToString(prefix, fields, nestedFieldKeys);
    // Retrieve fields from the superclasses recursively
    Class<?> superClass = clazz.getSuperclass();
    while (superClass != null && !superClass.isAssignableFrom(Object.class)) {
      fields = superClass.getDeclaredFields();
      formatFieldsToString(prefix, fields, nestedFieldKeys);
      superClass = superClass.getSuperclass();
    }

    return nestedFieldKeys;
  }

  public static void formatFieldsToString(
    String prefix,
    Field[] fields,
    List<String> nestedFieldKeys
  ) {
    for (Field field : fields) {
      field.setAccessible(true);

      if (!IEntityDto.class.isAssignableFrom(field.getType())) {
        nestedFieldKeys.add(
          prefix.isBlank() ? field.getName() : STR."\{prefix}.\{field.getName()}"
        );
      } else {
        List<String> childNestedFieldKeys = getClassFields(
          field.getType(),
          prefix.isBlank() ? field.getName() : STR."\{prefix}.\{field.getName()}"
        );
        nestedFieldKeys.addAll(childNestedFieldKeys);
      }
    }
  }

  public static Class<?> getFieldClass(Class<?> parentDtoType, String field) {
    Class<?> fieldType = getDeclaredField(parentDtoType, field).getType();
    fieldType = getListNestedClass(parentDtoType, field, fieldType);
    return fieldType;
  }

  public static Field getDeclaredField(Class<?> parentDtoType, String field) {
    try {
      var containsField = Arrays
        .stream(parentDtoType.getDeclaredFields())
        .anyMatch(parentDtoField -> parentDtoField.getName().equals(field));
      if (!containsField) {
        Class<?> superDtoType = parentDtoType.getSuperclass();
        if (
          superDtoType != null && !superDtoType.isAssignableFrom(Object.class)
        ) {
          return getDeclaredField(superDtoType, field);
        } else {
          throw new NoSuchFieldException();
        }
      }
      return parentDtoType.getDeclaredField(field);
    } catch (NoSuchFieldException e) {
      throw new RuntimeException(
          STR."Failed to set value for field: \{field} in \{parentDtoType.getName()}",
        e
      );
    }
  }

  public static Class<?> getListNestedClass(
    Class<?> parentDtoType,
    String field,
    Class<?> fieldType
  ) {
    try {
      if (Iterable.class.isAssignableFrom(fieldType)) {
        fieldType =
          (Class<?>) (
            (ParameterizedType) parentDtoType
              .getDeclaredField(field)
              .getGenericType()
          ).getActualTypeArguments()[0];
      }
      return fieldType;
    } catch (NoSuchFieldException e) {
      throw new RuntimeException(
          STR."Failed to set value for field: \{field} in \{parentDtoType.getName()}",
        e
      );
    }
  }

  public static Optional<FormattedField> getFieldByFullPath(
    List<FormattedField> fields,
    String fullPath
  ) {
    return getFieldByFullPath(fields, fullPath, new ArrayList<>());
  }

  public static Optional<FormattedField> getFieldByFullPath(
    List<FormattedField> fields,
    String fullPath,
    List<FormattedField> availableFields
  ) {
    for (FormattedField field : fields) {
      if (!field.subfields().isEmpty()) {
        getFieldByFullPath(field.subfields(), fullPath, availableFields);
      }
      availableFields.add(field);
    }
    return availableFields
      .stream()
      .filter(availableField -> availableField.fullField().equals(fullPath))
      .findFirst();
  }
}
