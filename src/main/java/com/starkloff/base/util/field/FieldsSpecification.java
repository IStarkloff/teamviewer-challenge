package com.starkloff.base.util.field;

import jakarta.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;

public class FieldsSpecification<T> implements Specification<T> {

  private final List<String> fieldNames;

  public FieldsSpecification(List<String> fieldNames) {
    this.fieldNames = fieldNames;
  }

  @Override
  public Predicate toPredicate(
    Root<T> root,
    CriteriaQuery<?> query,
    CriteriaBuilder builder
  ) {
    List<Predicate> predicates = new ArrayList<>();
    for (String fieldName : fieldNames) {
      String[] nestedAttributes = fieldName.split("\\.");
      Path<Object> path = root.get(nestedAttributes[0]);
      for (int i = 1; i < nestedAttributes.length; i++) {
        path = path.get(nestedAttributes[i]);
      }
      predicates.add(builder.isNotNull(path));
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }
}
