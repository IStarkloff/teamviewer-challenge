package com.starkloff.base.util.field;

import java.util.List;

public record FormattedField(
  String field, List<FormattedField> subfields, String fullField, Class<?> parentDtoType, Class<?> dtoType
) {}
