package com.starkloff.base.util.collection;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class Streams {

  private Streams() {}

  public static <T> Stream<T> from(Iterable<T> iterable) {
    return from(iterable, false);
  }

  public static <T> Stream<T> from(Iterable<T> iterable, boolean parallel) {
    return iterable == null
      ? Stream.empty()
      : StreamSupport.stream(iterable.spliterator(), parallel);
  }
}
