package com.starkloff.base.util;

import com.starkloff.base.model.IEntity;
import com.starkloff.base.util.field.FieldUtil;
import jakarta.persistence.criteria.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;

public class ProjectionUtil {

  public static <T> Specification<T> createProjectionSpecification(
    List<String> fields
  ) {
    return (root, query, criteriaBuilder) -> {
      if (fields == null || fields.isEmpty()) {
        query.multiselect(root);
      } else {
        List<Selection<?>> selections = new ArrayList<>();
        for (String field : fields) {
          Path<?> attributePath = getAttributePath(root, field, query);
          selections.add(attributePath.alias(field));
        }

        query.multiselect(selections);
      }
      return criteriaBuilder.and(); // Return the predicate to apply additional conditions
    };
  }

  private static <T> Path<?> getAttributePath(
    Root<T> root,
    String field,
    CriteriaQuery<?> query
  ) {
    // Split the field by dots to handle nested object attributes
    String[] fieldParts = field.split("\\.");

    Path<?> attributePath = root;
    Join<?, ?> nestedJoin = null;
    for (String part : fieldParts) {
      Field fieldObj = FieldUtil.getDeclaredField(
        attributePath.getJavaType(),
        part
      );
      if (IEntity.class.isAssignableFrom(fieldObj.getType())) {
        nestedJoin =
          nestedJoin != null
            ? nestedJoin.join(part, JoinType.LEFT)
            : root.join(part, JoinType.LEFT);
      }
      if (Iterable.class.isAssignableFrom(fieldObj.getType())) {
        assert nestedJoin != null;
        attributePath = nestedJoin.get(part);
      } else {
        attributePath = attributePath.get(part);
      }
    }
    return attributePath;
  }
}
