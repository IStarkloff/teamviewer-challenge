package com.starkloff.base.controller;

import com.starkloff.base.service.dto.ErrorResponseDto;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class RestExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

  private static ErrorResponseDto getErrorAttributes(HttpServletRequest request, Throwable error, HttpStatus status) {

    LOGGER.error(STR."\{error.getClass().getSimpleName()} exception caught", error);

    var errorResponse = new ErrorResponseDto();

    errorResponse.setException(error.getClass().getName());

    if (error.getLocalizedMessage() != null && !error.getLocalizedMessage().isBlank()) {
      errorResponse.setMessage(error.getLocalizedMessage());
    }

    errorResponse.setStatus(status.value());
    errorResponse.setError(status.getReasonPhrase());
    errorResponse.setPath(request.getRequestURI());

    return errorResponse;
  }

  private static String buildConstraintViolationMessage(ConstraintViolation<?> cv) {
    return cv.getMessage();
  }

  private static String buildBindViolationMessage(ObjectError oe) {
    return oe.getDefaultMessage();
  }

  @ExceptionHandler(value = {ValidationException.class })
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public @ResponseBody ErrorResponseDto handleBadRequestValidationException(final ValidationException exception, final HttpServletRequest request) {
    var error = getErrorAttributes(request, exception, HttpStatus.BAD_REQUEST);
    if (exception instanceof ConstraintViolationException constraintViolationException) {
      error.setConstraintViolations(constraintViolationException
          .getConstraintViolations().stream()
          .map(RestExceptionHandler::buildConstraintViolationMessage)
          .toList());
    } else {
      error.setConstraintViolations(List.of(exception.getMessage()));
    }
    return error;
  }

  @ExceptionHandler(value = {BindException.class })
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public @ResponseBody ErrorResponseDto handleBadRequest(final BindException exception, final HttpServletRequest request) {
    var error = getErrorAttributes(request, exception, HttpStatus.BAD_REQUEST);
    error.setConstraintViolations(exception
        .getAllErrors().stream()
        .map(RestExceptionHandler::buildBindViolationMessage)
        .toList());
    return error;
  }

  @ExceptionHandler(value = {NoSuchElementException.class})
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public @ResponseBody ErrorResponseDto handleResourceNotFound(final NoSuchElementException exception, final HttpServletRequest request) {
    return getErrorAttributes(request, exception, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler()
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public @ResponseBody ErrorResponseDto handleException(final Exception exception, final HttpServletRequest request) {
    return getErrorAttributes(request, exception, HttpStatus.INTERNAL_SERVER_ERROR);
  }


}
