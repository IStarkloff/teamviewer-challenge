package com.starkloff.base.controller;

import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import jakarta.validation.Valid;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface UpdateController<TDto extends IEntityDto, TEntityService extends EntityService<TDto>> {
  @NonNull
  TEntityService getEntityService();

  @PutMapping
  default ResponseEntity<TDto> update(@RequestBody @Valid TDto dto) {
    return ResponseEntity.ok(getEntityService().update(dto));
  }
}
