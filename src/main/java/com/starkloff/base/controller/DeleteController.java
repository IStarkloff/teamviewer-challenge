package com.starkloff.base.controller;

import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface DeleteController<TDto extends IEntityDto, TEntityService extends EntityService<TDto>> {
  @NonNull
  TEntityService getEntityService();

  @DeleteMapping("{id}")
  default ResponseEntity<Void> deleteById(@PathVariable long id) {
    getEntityService().deleteById(id);
    return ResponseEntity.ok(null);
  }
  /*@DeleteMapping("ids")
  default ResponseEntity<Void> deleteByIds(@RequestParam("ids") List<Long> ids) {
    getEntityService().deleteByIds(ids);
    return ResponseEntity.ok(null);
  }*/
}
