package com.starkloff.base.controller;

import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import jakarta.validation.Valid;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface CreateController<TDto extends IEntityDto, TEntityService extends EntityService<TDto>> {
  @NonNull
  TEntityService getEntityService();

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  default ResponseEntity<Void> create(@RequestBody @Valid TDto dto) {
    getEntityService().create(dto);
    return new ResponseEntity<>(HttpStatus.CREATED);
  }
}
