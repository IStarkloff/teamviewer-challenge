package com.starkloff.base.controller;

import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public abstract class EntityController<TDto extends IEntityDto, TEntityService extends EntityService<TDto>>
  implements
    GetController<TDto, TEntityService>,
    CreateController<TDto, TEntityService>,
    UpdateController<TDto, TEntityService>,
    DeleteController<TDto, TEntityService> {

  protected final TEntityService entityService;

  @Override
  public @NonNull TEntityService getEntityService() {
    return entityService;
  }
}
