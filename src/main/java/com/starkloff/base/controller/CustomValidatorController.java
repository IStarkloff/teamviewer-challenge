package com.starkloff.base.controller;

import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.validation.EntityValidator;
import java.util.Optional;
import lombok.NonNull;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface CustomValidatorController<TDto extends IEntityDto, TValidator extends EntityValidator<TDto>> {
  @NonNull
  Class<TDto> getDtoType();

  @NonNull
  TValidator getValidator();

  @InitBinder
  default void initBcConfigurationValidator(WebDataBinder binder) {
    Optional
      .ofNullable(binder.getTarget())
      .filter(notNullBinder -> getDtoType().equals(notNullBinder.getClass()))
      .ifPresent(o -> binder.addValidators(getValidator()));
  }
}
