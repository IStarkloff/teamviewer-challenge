package com.starkloff.base.controller;

import com.starkloff.base.rsql_config.dto.FieldsRequest;
import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import java.util.List;
import lombok.NonNull;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public interface GetController<TDto extends IEntityDto, TEntityService extends EntityService<TDto>> {
  @NonNull
  TEntityService getEntityService();

  // I strongly don't recommend to expose an API to get all entities from a table, unless you know that there are going to be a limited number of entities.
  @GetMapping
  default ResponseEntity<List<TDto>> getAll() {
    return ResponseEntity.ok(getEntityService().getAll());
  }

  // I prefer to expose an API to get a page of entities
  @GetMapping("page")
  default ResponseEntity<PageDto<TDto>> getPage(
    @ParameterObject Pageable pageable
  ) {
    return ResponseEntity.ok(getEntityService().getPage(pageable));
  }

  @GetMapping("{id}")
  default ResponseEntity<TDto> getById(@PathVariable long id) {
    return ResponseEntity.ok(getEntityService().getById(id));
  }

  // I took the liberty of adding this API that allows the client to filter dynamically based on the entity attributes
  @PostMapping("filtered")
  default ResponseEntity<PageDto<TDto>> getFiltered(
    @RequestBody(required = false) FieldsRequest fieldsRequest,
    @RequestParam(required = false) String search,
    @ParameterObject Pageable pageable
  ) {
    return ResponseEntity.ok(
      getEntityService().getFiltered(search, fieldsRequest, pageable)
    );
  }
  /*@GetMapping("ids")
  default ResponseEntity<List<TDto>> getByIds(
      @RequestParam("ids") List<Long> ids
  ) {
    return ResponseEntity.ok(getEntityService().getByIds(ids));
  }*/
}
