package com.starkloff.base.data.impl;

import com.starkloff.base.model.ICreatedAtEntity;
import jakarta.persistence.PrePersist;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class CreatedAtListener {

  @PrePersist
  public void onPrePersist(Object entity) {
    if (entity instanceof ICreatedAtEntity createdAtEntity) {
      if (createdAtEntity.getCreatedAt() == null) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
          "yyyy-MM-dd:HH:mm:ss"
        );
        String formattedDateTime = formatter.format(
          LocalDateTime.now(ZoneOffset.UTC)
        );
        createdAtEntity.setCreatedAt(
          LocalDateTime.parse(formattedDateTime, formatter)
        );
      }
    }
  }
}
