package com.starkloff.base.data.impl;

import com.querydsl.core.JoinExpression;
import com.querydsl.core.JoinType;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.jpa.JPQLSerializer;
import com.querydsl.jpa.impl.JPAProvider;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAUtil;
import com.starkloff.base.data.EntityRepository;
import com.starkloff.base.model.IEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

public abstract class EntityRepositoryImpl<T extends IEntity>
  extends SimpleJpaRepository<T, Long>
  implements EntityRepository<T> {

  private static final ConcurrentHashMap<String, Field> ENTITY_PROPERTY_EXPRESSIONS = new ConcurrentHashMap<>();

  private final EntityManager entityManager;

  private final Class<T> entityType;

  protected EntityRepositoryImpl(
    Class<T> entityType,
    EntityManager entityManager
  ) {
    super(entityType, entityManager);
    this.entityManager = entityManager;
    this.entityType = entityType;
  }

  @Override
  public EntityManager getEntityManager() {
    return entityManager;
  }

  // TODO Fix bug select List attribute in a tuple
  @Override
  public <R> Page<R> findAll(
    @NonNull Specification<T> specification,
    Pageable pageable,
    Class<R> resultType
  ) {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<R> criteriaQuery = criteriaBuilder.createQuery(resultType);
    Root<T> root = criteriaQuery.from(entityType);
    Predicate predicate = specification.toPredicate(
      root,
      criteriaQuery,
      criteriaBuilder
    );

    criteriaQuery.where(predicate);

    if (pageable.getSort().isSorted()) {
      List<Order> orders = new ArrayList<>();
      for (Sort.Order order : pageable.getSort()) {
        orders.add(
          order.isAscending()
            ? criteriaBuilder.asc(root.get(order.getProperty()))
            : criteriaBuilder.desc(root.get(order.getProperty()))
        );
      }
      criteriaQuery.orderBy(orders);
    }

    TypedQuery<R> query = entityManager.createQuery(criteriaQuery);
    if (pageable.isPaged()) {
      query.setFirstResult((int) pageable.getOffset());
      query.setMaxResults(pageable.getPageSize());
    }
    List<R> results = query.getResultList();
    Page<R> page = PageableExecutionUtils.getPage(
      results,
      pageable,
      () ->
        executeCountQuery(this.getCountQuery(specification, getDomainClass()))
    );

    return pageable.isUnpaged() ? new PageImpl<>(results) : page;
  }

  private static long executeCountQuery(TypedQuery<Long> query) {
    Assert.notNull(query, "TypedQuery must not be null!");
    List<Long> totals = query.getResultList();
    long total = 0L;

    Long element;
    for (
      Iterator<Long> var3 = totals.iterator();
      var3.hasNext();
      total = total + (element == null ? 0L : element)
    ) {
      element = var3.next();
    }

    return total;
  }

  protected JPAQuery<T> buildRootJPAQuery() {
    return buildJPAQuery(getDomainClass());
  }

  protected <T2 extends IEntity> JPAQuery<T2> buildJPAQuery(
    Class<T2> entityType
  ) {
    JPAQuery<T2> query = new JPAQuery<>(getEntityManager());
    EntityPath<T2> path = new EntityPathBase<T2>(
      entityType,
      entityType.getSimpleName()
    );
    return query.from(path);
  }

  protected <T2> Page<T2> fetchQueryPage(
    JPAQuery<T2> query,
    Pageable pageable
  ) {
    var pageResults = fetchQueryPageItems(query, pageable);
    long totalCount = fetchQueryCount(query);
    return buildPage(pageable, pageResults, totalCount);
  }

  private <T2> long fetchQueryCount(JPAQuery<T2> query) {
    var jpqlSerializer = new JPQLSerializer(
      JPAProvider.getTemplates(entityManager),
      entityManager
    );
    jpqlSerializer.serialize(query.getMetadata(), true, null);
    var jpqlQuery = jpqlSerializer.toString();
    var jpaQuery = entityManager.createQuery(jpqlQuery);
    JPAUtil.setConstants(
      jpaQuery,
      jpqlSerializer.getConstants(),
      query.getMetadata().getParams()
    );
    return (long) jpaQuery.getSingleResult();
  }

  private <T2> List<T2> fetchQueryPageItems(
    JPAQuery<T2> query,
    Pageable pageable
  ) {
    if (pageable.isUnpaged()) {
      return query.fetch();
    }

    query =
      query.clone().offset(pageable.getOffset()).limit(pageable.getPageSize());

    if (pageable.getSort().isSorted()) {
      applyQuerySorting(query, pageable);
    }

    return query.fetch();
  }

  private <T2> void applyQuerySorting(JPAQuery<T2> query, Pageable pageable) {
    var rootExpression = query
      .getMetadata()
      .getJoins()
      .stream()
      .filter(j -> j.getType() == JoinType.DEFAULT)
      .map(JoinExpression::getTarget)
      .findFirst()
      .orElse(null);

    if (rootExpression == null) {
      throw new UnsupportedOperationException(
        "Could not resolve query root target expression to evaluate sorts"
      );
    }

    query.orderBy(
      pageable
        .getSort()
        .get()
        .map(pageableOrder ->
          buildOrderSpecifier(
            rootExpression,
            pageableOrder.getProperty(),
            toQueryDslOrder(pageableOrder),
            toQueryDslNullHandling(pageableOrder)
          )
        )
        .toArray(OrderSpecifier<?>[]::new)
    );
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  protected <T2> OrderSpecifier<?> buildOrderSpecifier(
    com.querydsl.core.types.Expression<?> target,
    String propertyName,
    com.querydsl.core.types.Order order,
    OrderSpecifier.NullHandling nullHandling
  ) {
    var propertyExpression = findEntityPropertyExpression(target, propertyName);
    return new OrderSpecifier(order, propertyExpression, nullHandling);
  }

  @SuppressWarnings("rawtypes")
  private static com.querydsl.core.types.Expression findEntityPropertyExpression(
    com.querydsl.core.types.Expression<?> target,
    String propertyName
  ) {
    try {
      var targetClass = target.getClass(); // the Query DSL entity class (ie, Class<? extends QEntity>)

      var targetPropertyField = ENTITY_PROPERTY_EXPRESSIONS.computeIfAbsent(
        targetClass.getName() + ":" + propertyName,
        key -> {
          try {
            return targetClass.getField(propertyName);
          } catch (NoSuchFieldException ex) {
            return null;
          }
        }
      );

      if (targetPropertyField == null) {
        throw new UnsupportedOperationException(
          String.format(
            "Could not resolve expression for entity %s and property %s",
            target.getType(),
            propertyName
          )
        );
      }

      return (com.querydsl.core.types.Expression) targetPropertyField.get(
        target
      );
    } catch (IllegalAccessException e) {
      throw new UnsupportedOperationException(
        String.format(
          "Could not resolve expression for entity %s and property %s",
          target.getType(),
          propertyName
        )
      );
    }
  }

  private static com.querydsl.core.types.Order toQueryDslOrder(Sort.Order o) {
    return o.getDirection() == Sort.Direction.ASC
      ? com.querydsl.core.types.Order.ASC
      : com.querydsl.core.types.Order.DESC;
  }

  private static OrderSpecifier.NullHandling toQueryDslNullHandling(
    Sort.Order o
  ) {
    return o.getNullHandling() == Sort.NullHandling.NULLS_FIRST
      ? OrderSpecifier.NullHandling.NullsFirst
      : o.getNullHandling() == Sort.NullHandling.NULLS_LAST
        ? OrderSpecifier.NullHandling.NullsLast
        : OrderSpecifier.NullHandling.Default;
  }

  protected static <T> Page<T> buildPage(
    Pageable pageable,
    List<T> results,
    long totalResultsCount
  ) {
    return new PageImpl<>(results, pageable, totalResultsCount);
  }

  protected Page<T> fetchQueryPage(
    JPAQuery<T> query,
    Pageable pageable,
    Long totalCount
  ) {
    List<T> pageResults = fetchQueryPageItems(query, pageable);
    return buildPage(pageable, pageResults, totalCount);
  }
}
