package com.starkloff.base.data.impl;

import com.starkloff.base.model.IUpdatedAtEntity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class UpdatedAtListener {

  @PrePersist
  @PreUpdate
  public void onPreUpdate(Object entity) {
    if (entity instanceof IUpdatedAtEntity updatedAtEntity) {
      updatedAtEntity.setUpdatedAt(LocalDateTime.now(ZoneOffset.UTC));
    }
  }
}
