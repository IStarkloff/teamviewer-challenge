package com.starkloff.base.data;

import com.starkloff.base.model.IEntity;
import jakarta.persistence.EntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface EntityRepository<T extends IEntity>
  extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
  EntityManager getEntityManager();

  // TODO Fix bug select List attribute in a tuple
  <R> Page<R> findAll(
    Specification<T> specification,
    Pageable pageable,
    Class<R> resultClass
  );
}
