package com.starkloff.base.service.dto;

import com.starkloff.base.validation.EntityValidator;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RelatedEntityValidator {

  private EntityValidator<?> entityValidator;
  private List<?> entitiesToValidate;
}
