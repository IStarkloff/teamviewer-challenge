package com.starkloff.base.service.dto;

import java.io.Serial;

public abstract class BaseEntityDto implements IEntityDto {

  @Serial
  private static final long serialVersionUID = -8314603297311949946L;

  private long id;

  protected BaseEntityDto() {}

  protected BaseEntityDto(long id) {
    this.id = id;
  }

  @Override
  public long getId() {
    return id;
  }

  @Override
  public void setId(long id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (!isPersisted()) {
      return false;
    }

    if (!getClass().isInstance(obj)) {
      return false;
    }

    BaseEntityDto that = (BaseEntityDto) obj;
    return getId() == that.getId();
  }

  @Override
  public int hashCode() {
    return isPersisted() ? Long.hashCode(getId()) : super.hashCode();
  }

  @Override
  public String toString() {
    return getClass().getName() + "(id=" + getId() + ")";
  }
}
