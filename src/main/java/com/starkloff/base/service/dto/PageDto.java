package com.starkloff.base.service.dto;

import java.io.Serial;
import java.util.List;
import lombok.Data;

@Data
public class PageDto<T extends IDto> implements IDto {

  @Serial
  private static final long serialVersionUID = -8817479918094953905L;

  private int totalItems;
  private int totalPages;

  private int pageNumber;

  private int pageSize;

  private List<T> items;

  public PageDto() {}

  public PageDto(
    int totalItems,
    int totalPages,
    int pageNumber,
    int pageSize,
    List<T> items
  ) {
    this.totalItems = totalItems;
    this.totalPages = totalPages;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.items = items;
  }

  @Override
  public String toString() {
    return (
      "PageDto(totalItems=" +
      getTotalItems() +
      ", pageSize=" +
      getPageSize() +
      ", pageNumber=" +
      getPageNumber() +
      ", items=" +
      getItems() +
      ")"
    );
  }
}
