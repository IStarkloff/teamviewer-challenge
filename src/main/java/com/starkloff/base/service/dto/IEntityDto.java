package com.starkloff.base.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IEntityDto extends IDto {
  long getId();

  void setId(long id);

  @JsonIgnore
  default boolean isPersisted() {
    return getId() != 0L;
  }

  @JsonIgnore
  default String getUniqueId() {
    return getClass().getName() + ":" + getId();
  }
}
