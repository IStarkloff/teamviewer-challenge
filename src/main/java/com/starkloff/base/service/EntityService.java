package com.starkloff.base.service;

import com.starkloff.base.rsql_config.dto.FieldsRequest;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

public interface EntityService<T extends IEntityDto> {
  List<T> getAll();

  PageDto<T> getPage(Pageable pageable);

  PageDto<T> getPage(@Nullable FieldsRequest fieldsRequest, Pageable pageable);

  PageDto<T> getFiltered(
    String search,
    @Nullable FieldsRequest fieldsRequest,
    Pageable pageable
  );

  T getById(long id);

  List<T> getByIds(List<Long> ids);

  T create(T entityDto);

  List<T> createAll(List<T> entitiesDto);

  T update(T entityDto);

  void deleteById(long id);

  void deleteByIds(List<Long> ids);
}
