package com.starkloff.base.service;

import java.io.Serial;

public class ServiceException extends RuntimeException {

  @Serial
  private static final long serialVersionUID = 3771821614454631372L;

  public ServiceException() {}

  public ServiceException(String message) {
    super(message);
  }

  public ServiceException(String message, Exception innerException) {
    super(message, innerException);
  }
}
