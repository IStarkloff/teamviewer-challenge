package com.starkloff.base.service.impl;

import com.starkloff.base.data.EntityRepository;
import com.starkloff.base.mapper.EntityMapper;
import com.starkloff.base.model.IEntity;
import com.starkloff.base.rsql_config.CustomRsqlVisitor;
import com.starkloff.base.rsql_config.dto.FieldsRequest;
import com.starkloff.base.service.EntityService;
import com.starkloff.base.service.dto.IEntityDto;
import com.starkloff.base.service.dto.PageDto;
import com.starkloff.base.util.ProjectionUtil;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import jakarta.persistence.Tuple;
import jakarta.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

public abstract class EntityServiceImpl<TModel extends IEntity, TDto extends IEntityDto, TRepository extends EntityRepository<TModel>, TMapper extends EntityMapper<TModel, TDto>>
    implements EntityService<TDto> {

  protected final transient Logger logger = LoggerFactory.getLogger(getClass());

  protected final TRepository repository;
  protected final TMapper mapper;

  protected EntityServiceImpl(TRepository repository, TMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  protected Class<TDto> getDtoType() {
    return mapper.getDtoType();
  }

  protected Class<TModel> getModelType() {
    return mapper.getModelType();
  }

  @Override
  public List<TDto> getAll() {
    List<TModel> entities = repository.findAll();
    return mapper.toDtos(entities).toList();
  }

  @Override
  public PageDto<TDto> getPage(Pageable pageable) {
    Page<TModel> entitiesPage = repository.findAll(
        pageable
    );
    return mapper.toPageDto(entitiesPage);
  }

  @Override
  public PageDto<TDto> getPage(
      @Nullable FieldsRequest fieldsRequest,
      Pageable pageable
  ) {
    if (fieldsRequest == null || fieldsRequest.getFields().isEmpty()) {
      return getPage(pageable);
    }
    Specification<TModel> specification = ProjectionUtil.createProjectionSpecification(
        fieldsRequest.getFields()
    );
    Page<Tuple> tuples = repository.findAll(
        specification,
        pageable,
        Tuple.class
    );

    return mapper.tuplesToPageDto(tuples);
  }

  @Override
  public PageDto<TDto> getFiltered(
      String search,
      @Nullable FieldsRequest fieldsRequest,
      Pageable pageable
  ) {
    Node rootNode = search == null || search.isEmpty()
        ? null
        : new RSQLParser().parse(search);
    return rootNode != null
        ? getFiltered(rootNode, fieldsRequest, pageable)
        : getPage(fieldsRequest, pageable);
  }

  public PageDto<TDto> getFiltered(
      Node rootNode,
      @Nullable FieldsRequest fieldsRequest,
      Pageable pageable
  ) {
    if (fieldsRequest == null || fieldsRequest.getFields().isEmpty()) {
      return getFiltered(rootNode, pageable);
    }
    Specification<TModel> specification = Specification.allOf(
        ProjectionUtil.createProjectionSpecification(fieldsRequest.getFields()),
        rootNode.accept(new CustomRsqlVisitor<>())
    );

    Page<Tuple> tuples = repository.findAll(
        specification,
        pageable,
        Tuple.class
    );

    return mapper.tuplesToPageDto(tuples);
  }

  public PageDto<TDto> getFiltered(Node rootNode, Pageable pageable) {
    Specification<TModel> specification = rootNode.accept(
        new CustomRsqlVisitor<>()
    );
    Page<TModel> entities = repository.findAll(
        specification,
        pageable
    );
    return mapper.toPageDto(entities);
  }

  @Override
  public TDto getById(long id) {
    return mapper.toDto(
        repository
            .findById(id)
            .orElseThrow(() ->
                new NoSuchElementException(
                    STR."There is no \{getModelType().getSimpleName()} with ID: \{id}"
                )
            )
    );
  }

  @Override
  public List<TDto> getByIds(List<Long> ids) {
    return mapper
        .toDtos(repository.findAllById(ids))
        .toList();
  }

  @Override
  @Transactional
  public TDto create(
      TDto entityDto
  ) {
    if (entityDto.getId() != 0L) {
      throw new ValidationException(
          "ID of entity to create must not be initialized"
      );
    }
    TModel entity = mapper.toModel(entityDto);

    var savedEntity = repository.save(entity);

    return mapper.toDto(savedEntity);
  }

  @Override
  public List<TDto> createAll(List<TDto> entitiesDto) {
    if (entitiesDto.stream().anyMatch(entityDto -> entityDto.getId() != 0L)) {
      throw new ValidationException(
          "ID of entity to create must not be initialized"
      );
    }
    var entities = mapper.toModels(entitiesDto).toList();

    var savedEntities = repository.saveAll(entities);

    return mapper.toDtos(savedEntities).toList();
  }

  @Override
  @Transactional
  public TDto update(
      TDto entityDto
  ) {
    TModel entity = repository.findById(entityDto.getId()).orElseThrow(() -> new NoSuchElementException(
        STR."There is no \{getModelType().getSimpleName()} with ID: \{entityDto.getId()}"
    ));

    mapper.toModel(entityDto, entity);

    var savedEntity = repository.save(entity);

    return mapper.toDto(savedEntity);
  }

  @Override
  @Transactional
  public void deleteById(long id) {
    if (!repository.existsById(id)) {
      throw new NoSuchElementException(
          STR."There is no \{getModelType().getSimpleName()} with ID: \{id}"
      );
    }
    repository.deleteById(id);
  }

  @Override
  @Transactional
  public void deleteByIds(List<Long> ids) {
    for (var id : ids) {
      deleteById(id);
    }
  }
}
