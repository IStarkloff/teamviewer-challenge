package com.starkloff.base.model;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

public interface IUpdatedAtEntity extends IEntity {
  LocalDateTime getUpdatedAt();

  void setUpdatedAt(@NotNull LocalDateTime updatedAt);
}
