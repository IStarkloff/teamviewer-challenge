package com.starkloff.base.model;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

public interface ICreatedAtEntity extends IEntity {
  LocalDateTime getCreatedAt();

  void setCreatedAt(@NotNull LocalDateTime createdAt);
}
