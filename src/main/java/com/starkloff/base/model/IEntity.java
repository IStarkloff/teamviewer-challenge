package com.starkloff.base.model;

public interface IEntity {
  long getId();

  void setId(long id);

  default boolean isPersisted() {
    return getId() != 0L;
  }

  /**
   * Gets the type used for comparing entities.
   *
   * @return the type used for comparing entities.
   */
  Class<? extends IEntity> realType();
}
