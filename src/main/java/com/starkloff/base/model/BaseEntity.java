package com.starkloff.base.model;

import com.starkloff.base.data.impl.CreatedAtListener;
import com.starkloff.base.data.impl.UpdatedAtListener;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners({ CreatedAtListener.class })
public abstract class BaseEntity implements IEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Override
  public long getId() {
    return id;
  }

  @Override
  public void setId(long id) {
    this.id = id;
  }

  @Override
  public abstract Class<? extends IEntity> realType();
}
