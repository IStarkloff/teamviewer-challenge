# TeamViewer Challenge APIs

This application allows users to perform CRUD (create, read, update, delete) operations on a data model representing a simple e-commerce platform.

## Installation

### Requirements

You will need:

- Java JDK 21 or higher
- Maven 3.9.6 or higher
- Docker
- Docker Compose

### Getting Started

Go to the location you want to save the project and execute:

```bash
git clone git@gitlab.com:IStarkloff/generic-crud.git
```

### Install Dependencies

Assuming you are in `teamviewer-challenge` folder, you must run the following command

```bash
mvn clean install -DskipTests
```

### Run configuration with Docker (RECOMMENDED)

#### Run application

Create a .env file based on the [.env_EXAMPLE](.env_EXAMPLE) file on the `teamviewer-challenge` folder.

```.dotenv
# WSL/LINUX/MAC
# =========
POSTGRES_PASSWORD=postgres
POSTGRES_USER=postgres
# set to your proper home directory
HOME=/home/your_user_name
# set to the path that contains the projects' repos
PROJECT_ROOT=${HOME}/projects
TEAMVIEWER_CHALLENGE_DIR=${PROJECTS_ROOT}/teamviewer-challenge
TEAMVIEWER_CHALLENGE_DB_DIR=${TEAMVIEWER_CHALLENGE_DIR}/database
M2_CONFIG=${HOME}/.m2
SPRING_PROFILES_ACTIVE_DEV=development
COMPOSE_DOCKER_CLI_BUILD=1

# WINDOWS
# =======
POSTGRES_PASSWORD=postgres
POSTGRES_USER=postgres
# set to your proper home directory
HOME=C:\Users\your_user_name
# set to the path that contains the projects' repos
PROJECT_ROOT=${HOME}\Documents\projects
TEAMVIEWER_CHALLENGE_DIR=${PROJECTS_ROOT}\teamviewer-challenge
TEAMVIEWER_CHALLENGE_DB_DIR=${TEAMVIEWER_CHALLENGE_DIR}\database
M2_CONFIG=${HOME}\.m2
SPRING_PROFILES_ACTIVE_DEV=development
```

The only attributes you probably need to update are:

#### WSL/LINUX/MAC
```.dotenv
# set to your proper home directory
HOME=/home/your_user_name
# set to the path that contains the projects' repos
PROJECTS_ROOT=${HOME}/Documents/projects
```

#### WINDOWS
```.dotenv
# set to your proper home directory
HOME=C:\Users\your_user_name
# set to the path that contains the projects' repos
PROJECT_ROOT=${HOME}\Documents\projects
```

Assuming you are in `teamviewer-challenge` folder, you must run the following command:

```bash
docker compose up --build
```

This application has hot-reload feature, so you don't need to restart the backend every time you change the code (except when you change something from the resources or the test folders)

Now you should be able to use this application on http://localhost:8021/ (**IMPORTANT**: If the application fails to start, it may be because the database `teamviewer_challenge` was not created, in which case you will have to create it manually, the database is running on the port 5421)

#### Turn on the debug mode

Uncomment these two lines from the docker-compose.yaml file

```
command: sh ./docker-entrypoint-debug.sh
- "5021:5021" # for debug, pay attention to address=5021 witch is the port where we expose the debugger
```

#### Execute tests

Inside the docker-compose.yaml file there is a service called `teamviewer-challenge-api-tests`, you can start it with the command `docker compose up teamviewer-challenge-api-tests` and see the tests results

#### Docker compose commands

#### Build all the services

```bash
docker compose build
```

#### Start all the services

```bash
docker compose up
```

#### Start a specific service

```bash
docker compose up ${service-name}
```

If you want to do a build too, you can do:

```bash
docker compose up --build ${service-name}
```

### Run tests

```bash
mvn test
```

### Run Configuration Locally (I strongly recommend use docker directly)

#### Start application

Assuming you already turn on the database and you are using java 21 (I recommend to use [sdkman](https://sdkman.io/) as a version manager)
You should run this command in the `teamviewer-challenge` folder:

```bash
mvn clean spring-boot:run -Dspring-boot.run.jvmArguments="--enable-preview" -Ddelete-schema-files=true -Dspring-boot.run.profiles=development
```

#### Execute tests

```bash
mvn test
```

#### Documentation
 
We are using OpenAPI v3 to handle the APIs documentation

If the project is already working on the port 8021, you can go to this link [Swagger UI](http://localhost:8021/teamviewer-challenge-service/swagger-ui/index.html#/) to get access to the APIs documentation

You can access to the OpenAPI json format, you can use these links [OpenAPI v3 JSON](http://localhost:8021/teamviewer-challenge-service/v3/api-docs) [OpenAPI v3 YAML](http://localhost:8021/teamviewer-challenge-service/v3/api-docs)

If you don't want to run this project to see the APIs documentation, you can go to this link [teamviewer-challenge-openapi.md](openAPI-docs%2Fteamviewer-challenge-openapi.md)

### RSQL (for /filtered APIs)

If you want to know how it works, please go to this link [RSQL](src%2Fmain%2Fjava%2Fcom%2Fstarkloff%2Fbase%2Frsql_config%2Frsql_builder%2FREADME.md)