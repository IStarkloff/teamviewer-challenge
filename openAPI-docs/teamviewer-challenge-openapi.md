---
title: TeamViewer Challenge APIs v0.0.1-SNAPSHOT
language_tabs: []
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<!-- Generator: Widdershins v4.0.1 -->

<h1 id="teamviewer-challenge-apis">TeamViewer Challenge APIs v0.0.1-SNAPSHOT</h1>

> Scroll down for example requests and responses.

This application allows users to perform CRUD (create, read, update, delete) operations on a data model representing a simple e-commerce platform.

Base URLs:

* <a href="http://localhost:8021/teamviewer-challenge-service">http://localhost:8021/teamviewer-challenge-service</a>

<h1 id="teamviewer-challenge-apis-product-controller">product-controller</h1>

## getAll

<a id="opIdgetAll"></a>

`GET /api/v1/products`

<h3 id="getall-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|Inline|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<h3 id="getall-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[ProductDto](#schemaproductdto)]|false|none|none|
|» id|integer(int64)|false|none|none|
|» price|number|false|none|none|
|» name|string|true|none|none|
|» brand|string|true|none|none|
|» createdAt|string(date-time)|false|read-only|none|

<aside class="success">
This operation does not require authentication
</aside>

## update

<a id="opIdupdate"></a>

`PUT /api/v1/products`

> Body parameter

```json
{
  "id": 0,
  "price": 0,
  "name": "string",
  "brand": "string"
}
```

<h3 id="update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[ProductDto](#schemaproductdto)|true|none|

<h3 id="update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[ProductDto](#schemaproductdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## create

<a id="opIdcreate"></a>

`POST /api/v1/products`

> Body parameter

```json
{
  "id": 0,
  "price": 0,
  "name": "string",
  "brand": "string"
}
```

<h3 id="create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[ProductDto](#schemaproductdto)|true|none|

<h3 id="create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getFiltered

<a id="opIdgetFiltered"></a>

`POST /api/v1/products/filtered`

> Body parameter

```json
{
  "fields": [
    "string"
  ]
}
```

<h3 id="getfiltered-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|search|query|string|false|none|
|page|query|integer|false|One-based page index (1..N)|
|size|query|integer|false|The size of the page to be returned|
|sort|query|array[string]|false|Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.|
|body|body|[FieldsRequest](#schemafieldsrequest)|false|none|

<h3 id="getfiltered-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[PageDtoProductDto](#schemapagedtoproductdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getById

<a id="opIdgetById"></a>

`GET /api/v1/products/{id}`

<h3 id="getbyid-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int64)|true|none|

<h3 id="getbyid-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[ProductDto](#schemaproductdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## deleteById

<a id="opIddeleteById"></a>

`DELETE /api/v1/products/{id}`

<h3 id="deletebyid-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int64)|true|none|

<h3 id="deletebyid-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getPage

<a id="opIdgetPage"></a>

`GET /api/v1/products/page`

<h3 id="getpage-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|page|query|integer|false|One-based page index (1..N)|
|size|query|integer|false|The size of the page to be returned|
|sort|query|array[string]|false|Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.|

<h3 id="getpage-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[PageDtoProductDto](#schemapagedtoproductdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

<h1 id="teamviewer-challenge-apis-order-controller">order-controller</h1>

## getAll_1

<a id="opIdgetAll_1"></a>

`GET /api/v1/orders`

<h3 id="getall_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|Inline|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<h3 id="getall_1-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[OrderDto](#schemaorderdto)]|false|none|none|
|» id|integer(int64)|false|none|none|
|» orderItems|[[OrderItemDto](#schemaorderitemdto)]|true|none|none|
|»» id|integer(int64)|false|none|none|
|»» price|number|false|none|none|
|»» order|[OrderDto](#schemaorderdto)|true|none|none|
|»» product|[ProductDto](#schemaproductdto)|true|none|none|
|»»» id|integer(int64)|false|none|none|
|»»» price|number|false|none|none|
|»»» name|string|true|none|none|
|»»» brand|string|true|none|none|
|»»» createdAt|string(date-time)|false|read-only|none|
|»» amount|integer(int32)|false|none|none|
|»» createdAt|string(date-time)|false|read-only|none|
|» createdAt|string(date-time)|false|read-only|none|

<aside class="success">
This operation does not require authentication
</aside>

## update_1

<a id="opIdupdate_1"></a>

`PUT /api/v1/orders`

> Body parameter

```json
{
  "id": 0,
  "orderItems": [
    {
      "id": 0,
      "price": 0,
      "order": {
        "id": 0,
        "orderItems": []
      },
      "product": {
        "id": 0,
        "price": 0,
        "name": "string",
        "brand": "string"
      },
      "amount": 1
    }
  ]
}
```

<h3 id="update_1-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OrderDto](#schemaorderdto)|true|none|

<h3 id="update_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[OrderDto](#schemaorderdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## create_1

<a id="opIdcreate_1"></a>

`POST /api/v1/orders`

> Body parameter

```json
{
  "id": 0,
  "orderItems": [
    {
      "id": 0,
      "price": 0,
      "order": {
        "id": 0,
        "orderItems": []
      },
      "product": {
        "id": 0,
        "price": 0,
        "name": "string",
        "brand": "string"
      },
      "amount": 1
    }
  ]
}
```

<h3 id="create_1-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OrderDto](#schemaorderdto)|true|none|

<h3 id="create_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getFiltered_1

<a id="opIdgetFiltered_1"></a>

`POST /api/v1/orders/filtered`

> Body parameter

```json
{
  "fields": [
    "string"
  ]
}
```

<h3 id="getfiltered_1-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|search|query|string|false|none|
|page|query|integer|false|One-based page index (1..N)|
|size|query|integer|false|The size of the page to be returned|
|sort|query|array[string]|false|Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.|
|body|body|[FieldsRequest](#schemafieldsrequest)|false|none|

<h3 id="getfiltered_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[PageDtoOrderDto](#schemapagedtoorderdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getById_1

<a id="opIdgetById_1"></a>

`GET /api/v1/orders/{id}`

<h3 id="getbyid_1-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int64)|true|none|

<h3 id="getbyid_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[OrderDto](#schemaorderdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## deleteById_1

<a id="opIddeleteById_1"></a>

`DELETE /api/v1/orders/{id}`

<h3 id="deletebyid_1-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int64)|true|none|

<h3 id="deletebyid_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getPage_1

<a id="opIdgetPage_1"></a>

`GET /api/v1/orders/page`

<h3 id="getpage_1-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|page|query|integer|false|One-based page index (1..N)|
|size|query|integer|false|The size of the page to be returned|
|sort|query|array[string]|false|Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.|

<h3 id="getpage_1-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[PageDtoOrderDto](#schemapagedtoorderdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

<h1 id="teamviewer-challenge-apis-order-item-controller">order-item-controller</h1>

## getAll_2

<a id="opIdgetAll_2"></a>

`GET /api/v1/order-items`

<h3 id="getall_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|Inline|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<h3 id="getall_2-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[OrderItemDto](#schemaorderitemdto)]|false|none|none|
|» id|integer(int64)|false|none|none|
|» price|number|false|none|none|
|» order|[OrderDto](#schemaorderdto)|true|none|none|
|»» id|integer(int64)|false|none|none|
|»» orderItems|[[OrderItemDto](#schemaorderitemdto)]|true|none|none|
|»» createdAt|string(date-time)|false|read-only|none|
|» product|[ProductDto](#schemaproductdto)|true|none|none|
|»» id|integer(int64)|false|none|none|
|»» price|number|false|none|none|
|»» name|string|true|none|none|
|»» brand|string|true|none|none|
|»» createdAt|string(date-time)|false|read-only|none|
|» amount|integer(int32)|false|none|none|
|» createdAt|string(date-time)|false|read-only|none|

<aside class="success">
This operation does not require authentication
</aside>

## update_2

<a id="opIdupdate_2"></a>

`PUT /api/v1/order-items`

> Body parameter

```json
{
  "id": 0,
  "price": 0,
  "order": {
    "id": 0,
    "orderItems": [
      {
        "id": 0,
        "price": 0,
        "order": {},
        "product": {
          "id": 0,
          "price": 0,
          "name": "string",
          "brand": "string"
        },
        "amount": 1
      }
    ]
  },
  "product": {
    "id": 0,
    "price": 0,
    "name": "string",
    "brand": "string"
  },
  "amount": 1
}
```

<h3 id="update_2-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OrderItemDto](#schemaorderitemdto)|true|none|

<h3 id="update_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[OrderItemDto](#schemaorderitemdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## create_2

<a id="opIdcreate_2"></a>

`POST /api/v1/order-items`

> Body parameter

```json
{
  "id": 0,
  "price": 0,
  "order": {
    "id": 0,
    "orderItems": [
      {
        "id": 0,
        "price": 0,
        "order": {},
        "product": {
          "id": 0,
          "price": 0,
          "name": "string",
          "brand": "string"
        },
        "amount": 1
      }
    ]
  },
  "product": {
    "id": 0,
    "price": 0,
    "name": "string",
    "brand": "string"
  },
  "amount": 1
}
```

<h3 id="create_2-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OrderItemDto](#schemaorderitemdto)|true|none|

<h3 id="create_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getFiltered_2

<a id="opIdgetFiltered_2"></a>

`POST /api/v1/order-items/filtered`

> Body parameter

```json
{
  "fields": [
    "string"
  ]
}
```

<h3 id="getfiltered_2-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|search|query|string|false|none|
|page|query|integer|false|One-based page index (1..N)|
|size|query|integer|false|The size of the page to be returned|
|sort|query|array[string]|false|Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.|
|body|body|[FieldsRequest](#schemafieldsrequest)|false|none|

<h3 id="getfiltered_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[PageDtoOrderItemDto](#schemapagedtoorderitemdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getById_2

<a id="opIdgetById_2"></a>

`GET /api/v1/order-items/{id}`

<h3 id="getbyid_2-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int64)|true|none|

<h3 id="getbyid_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[OrderItemDto](#schemaorderitemdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## deleteById_2

<a id="opIddeleteById_2"></a>

`DELETE /api/v1/order-items/{id}`

<h3 id="deletebyid_2-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int64)|true|none|

<h3 id="deletebyid_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

## getPage_2

<a id="opIdgetPage_2"></a>

`GET /api/v1/order-items/page`

<h3 id="getpage_2-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|page|query|integer|false|One-based page index (1..N)|
|size|query|integer|false|The size of the page to be returned|
|sort|query|array[string]|false|Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.|

<h3 id="getpage_2-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[PageDtoOrderItemDto](#schemapagedtoorderitemdto)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|[ErrorResponseDto](#schemaerrorresponsedto)|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|[ErrorResponseDto](#schemaerrorresponsedto)|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal Server Error|[ErrorResponseDto](#schemaerrorresponsedto)|

<aside class="success">
This operation does not require authentication
</aside>

# Schemas

<h2 id="tocS_ErrorResponseDto">ErrorResponseDto</h2>
<!-- backwards compatibility -->
<a id="schemaerrorresponsedto"></a>
<a id="schema_ErrorResponseDto"></a>
<a id="tocSerrorresponsedto"></a>
<a id="tocserrorresponsedto"></a>

```json
{
  "exception": "string",
  "message": "string",
  "status": 0,
  "error": "string",
  "path": "string",
  "constraintViolations": [
    "string"
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|exception|string|false|none|none|
|message|string|false|none|none|
|status|integer(int32)|false|none|none|
|error|string|false|none|none|
|path|string|false|none|none|
|constraintViolations|[string]|false|none|none|

<h2 id="tocS_ProductDto">ProductDto</h2>
<!-- backwards compatibility -->
<a id="schemaproductdto"></a>
<a id="schema_ProductDto"></a>
<a id="tocSproductdto"></a>
<a id="tocsproductdto"></a>

```json
{
  "id": 0,
  "price": 0,
  "name": "string",
  "brand": "string",
  "createdAt": "2019-08-24T14:15:22Z"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int64)|false|none|none|
|price|number|false|none|none|
|name|string|true|none|none|
|brand|string|true|none|none|
|createdAt|string(date-time)|false|read-only|none|

<h2 id="tocS_OrderDto">OrderDto</h2>
<!-- backwards compatibility -->
<a id="schemaorderdto"></a>
<a id="schema_OrderDto"></a>
<a id="tocSorderdto"></a>
<a id="tocsorderdto"></a>

```json
{
  "id": 0,
  "orderItems": [
    {
      "id": 0,
      "price": 0,
      "order": {
        "id": 0,
        "orderItems": [],
        "createdAt": "2019-08-24T14:15:22Z"
      },
      "product": {
        "id": 0,
        "price": 0,
        "name": "string",
        "brand": "string",
        "createdAt": "2019-08-24T14:15:22Z"
      },
      "amount": 1,
      "createdAt": "2019-08-24T14:15:22Z"
    }
  ],
  "createdAt": "2019-08-24T14:15:22Z"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int64)|false|none|none|
|orderItems|[[OrderItemDto](#schemaorderitemdto)]|true|none|none|
|createdAt|string(date-time)|false|read-only|none|

<h2 id="tocS_OrderItemDto">OrderItemDto</h2>
<!-- backwards compatibility -->
<a id="schemaorderitemdto"></a>
<a id="schema_OrderItemDto"></a>
<a id="tocSorderitemdto"></a>
<a id="tocsorderitemdto"></a>

```json
{
  "id": 0,
  "price": 0,
  "order": {
    "id": 0,
    "orderItems": [
      {
        "id": 0,
        "price": 0,
        "order": {},
        "product": {
          "id": 0,
          "price": 0,
          "name": "string",
          "brand": "string",
          "createdAt": "2019-08-24T14:15:22Z"
        },
        "amount": 1,
        "createdAt": "2019-08-24T14:15:22Z"
      }
    ],
    "createdAt": "2019-08-24T14:15:22Z"
  },
  "product": {
    "id": 0,
    "price": 0,
    "name": "string",
    "brand": "string",
    "createdAt": "2019-08-24T14:15:22Z"
  },
  "amount": 1,
  "createdAt": "2019-08-24T14:15:22Z"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int64)|false|none|none|
|price|number|false|none|none|
|order|[OrderDto](#schemaorderdto)|true|none|none|
|product|[ProductDto](#schemaproductdto)|true|none|none|
|amount|integer(int32)|false|none|none|
|createdAt|string(date-time)|false|read-only|none|

<h2 id="tocS_FieldsRequest">FieldsRequest</h2>
<!-- backwards compatibility -->
<a id="schemafieldsrequest"></a>
<a id="schema_FieldsRequest"></a>
<a id="tocSfieldsrequest"></a>
<a id="tocsfieldsrequest"></a>

```json
{
  "fields": [
    "string"
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|fields|[string]|false|none|none|

<h2 id="tocS_PageDtoProductDto">PageDtoProductDto</h2>
<!-- backwards compatibility -->
<a id="schemapagedtoproductdto"></a>
<a id="schema_PageDtoProductDto"></a>
<a id="tocSpagedtoproductdto"></a>
<a id="tocspagedtoproductdto"></a>

```json
{
  "totalItems": 0,
  "totalPages": 0,
  "pageNumber": 0,
  "pageSize": 0,
  "items": [
    {
      "id": 0,
      "price": 0,
      "name": "string",
      "brand": "string",
      "createdAt": "2019-08-24T14:15:22Z"
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|totalItems|integer(int32)|false|none|none|
|totalPages|integer(int32)|false|none|none|
|pageNumber|integer(int32)|false|none|none|
|pageSize|integer(int32)|false|none|none|
|items|[[ProductDto](#schemaproductdto)]|false|none|none|

<h2 id="tocS_PageDtoOrderDto">PageDtoOrderDto</h2>
<!-- backwards compatibility -->
<a id="schemapagedtoorderdto"></a>
<a id="schema_PageDtoOrderDto"></a>
<a id="tocSpagedtoorderdto"></a>
<a id="tocspagedtoorderdto"></a>

```json
{
  "totalItems": 0,
  "totalPages": 0,
  "pageNumber": 0,
  "pageSize": 0,
  "items": [
    {
      "id": 0,
      "orderItems": [
        {
          "id": 0,
          "price": 0,
          "order": {},
          "product": {
            "id": 0,
            "price": 0,
            "name": "string",
            "brand": "string",
            "createdAt": "2019-08-24T14:15:22Z"
          },
          "amount": 1,
          "createdAt": "2019-08-24T14:15:22Z"
        }
      ],
      "createdAt": "2019-08-24T14:15:22Z"
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|totalItems|integer(int32)|false|none|none|
|totalPages|integer(int32)|false|none|none|
|pageNumber|integer(int32)|false|none|none|
|pageSize|integer(int32)|false|none|none|
|items|[[OrderDto](#schemaorderdto)]|false|none|none|

<h2 id="tocS_PageDtoOrderItemDto">PageDtoOrderItemDto</h2>
<!-- backwards compatibility -->
<a id="schemapagedtoorderitemdto"></a>
<a id="schema_PageDtoOrderItemDto"></a>
<a id="tocSpagedtoorderitemdto"></a>
<a id="tocspagedtoorderitemdto"></a>

```json
{
  "totalItems": 0,
  "totalPages": 0,
  "pageNumber": 0,
  "pageSize": 0,
  "items": [
    {
      "id": 0,
      "price": 0,
      "order": {
        "id": 0,
        "orderItems": [
          {}
        ],
        "createdAt": "2019-08-24T14:15:22Z"
      },
      "product": {
        "id": 0,
        "price": 0,
        "name": "string",
        "brand": "string",
        "createdAt": "2019-08-24T14:15:22Z"
      },
      "amount": 1,
      "createdAt": "2019-08-24T14:15:22Z"
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|totalItems|integer(int32)|false|none|none|
|totalPages|integer(int32)|false|none|none|
|pageNumber|integer(int32)|false|none|none|
|pageSize|integer(int32)|false|none|none|
|items|[[OrderItemDto](#schemaorderitemdto)]|false|none|none|
