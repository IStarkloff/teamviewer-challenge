#!/bin/bash

# Run spring boot app in background and expose port 5021 to be able to debug the service with our IDE
mvn clean spring-boot:run -Dspring-boot.run.jvmArguments="--enable-preview -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=*:5021" -Ddelete-schema-files=true &

while inotifywait -r ./src/main/java -e modify,create,delete,move; do
  mvn compile -Ddelete-schema-files=true;
done

